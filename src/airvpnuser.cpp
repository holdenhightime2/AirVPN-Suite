/*
 * airvpnuser.cpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2021 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "include/airvpnuser.hpp"

#include <fstream>
#include <syslog.h>
#include <sstream>

extern void global_log(std::string s);

AirVPNUser::AirVPNUser(std::string userName, std::string password)
{
    airVpnTools = new AirVPNTools();

    initializeUserData();

    airVPNUserName = userName;
    airVPNUserPassword = password;

    loadUserProfile();
}

AirVPNUser::~AirVPNUser()
{
}

void AirVPNUser::initializeUserData()
{
    airVPNUserName = "";
    airVPNUserPassword = "";
    airVPNUserCurrentProfile = "";

    initializeUserProfileData();
}

void AirVPNUser::initializeUserProfileData()
{
    userIsValid = false;
    userLoginFailed = true;
    expirationDate = "";
    daysToExpiration = 0;
    certificateAuthorityCertificate = "";
    tlsAuthKey = "";
    tlsCryptKey = "";
    sshKey = "";
    sshPpk = "";
    sslCertificate = "";

    userKey.clear();
}

std::string AirVPNUser::getUserIP()
{
    return userIP;
}

void AirVPNUser::setUserIP(std::string ip)
{
    userIP = ip;
}

std::string AirVPNUser::getUserCountry()
{
    return userCountry;
}

void AirVPNUser::setUserCountry(std::string country)
{
    userCountry = country;
}

float AirVPNUser::getUserLatitude()
{
    return userLatitude;
}

void AirVPNUser::setUserLatitude(float l)
{
    userLatitude = l;
}

float AirVPNUser::getUserLongitude()
{
    return userLongitude;
}

void AirVPNUser::setUserLongitude(float l)
{
    userLongitude = l;
}

bool AirVPNUser::isUserValid()
{
    return userIsValid;
}

std::string AirVPNUser::getExpirationDate()
{
    return airVPNSubscriptionExpirationDate;
}

void AirVPNUser::setExpirationDate(std::string e)
{
    airVPNSubscriptionExpirationDate = e;
}

int AirVPNUser::getDaysToExpiration()
{
    return daysToExpiration;
}

void AirVPNUser::setDaysToExpiration(int d)
{
    daysToExpiration = d;
}

std::string AirVPNUser::getCertificateAuthorityCertificate()
{
    return certificateAuthorityCertificate;
}

void AirVPNUser::setCertificateAuthorityCertificate(std::string c)
{
    certificateAuthorityCertificate = c;
}

std::string AirVPNUser::getTlsAuthKey()
{
    return tlsAuthKey;
}

void AirVPNUser::setTlsAuthKey(std::string t)
{
    tlsAuthKey = t;
}

std::string AirVPNUser::getTlsCryptKey()
{
    return tlsCryptKey;
}

void AirVPNUser::setTlsCryptKey(std::string t)
{
    tlsCryptKey = t;
}

std::string AirVPNUser::getSshKey()
{
    return sshKey;
}

void AirVPNUser::setSshKey(std::string s)
{
    sshKey = s;
}

std::string AirVPNUser::getSshPpk()
{
    return sshPpk;
}

void AirVPNUser::setSshPpk(std::string s)
{
    sshPpk = s;
}

std::string AirVPNUser::getSslCertificate()
{
    return sslCertificate;
}

void AirVPNUser::setSslCertificate(std::string s)
{
    sslCertificate = s;
}

std::map<std::string, AirVPNUser::UserKey> AirVPNUser::getUserKeys()
{
    return userKey;
}

void AirVPNUser::setUserKeys(std::map<std::string, AirVPNUser::UserKey> u)
{
    userKey = u;
}

AirVPNUser::UserKey AirVPNUser::getUserKey(std::string name)
{
    UserKey key;

    if(userKey.empty())
        return key;

    std::map<std::string, UserKey>::iterator it = userKey.find(name);

    if(it != userKey.end())
        key = it->second;

    return key;
}

void AirVPNUser::setUserKey(std::string name, UserKey u)
{
    userKey.insert(std::make_pair(name, u));
}

void AirVPNUser::addUserKey(std::string name, UserKey key)
{
    if(name.empty() || !getUserKey(name).name.empty())
        return;

    userKey.insert(std::make_pair(name, key));
}

std::vector<std::string> AirVPNUser::getUserKeyNames()
{
    std::vector<std::string> keyNames;

    if(userKey.size() > 0)
    {
        for(std::map<std::string, UserKey>::iterator it = userKey.begin(); it != userKey.end(); it++)
            keyNames.push_back(it->first);
    }

    return keyNames;
}

AirVPNUser::UserProfileType AirVPNUser::getUserProfileType()
{
    return userProfileType;
}

std::string AirVPNUser::getUserName()
{
    return airVPNUserName;
}

void AirVPNUser::setUserName(std::string u)
{
    airVPNUserName = u;
}

std::string AirVPNUser::getUserPassword()
{
    return airVPNUserPassword;
}

void AirVPNUser::setUserPassword(std::string p)
{
    airVPNUserPassword = p;
}

std::string AirVPNUser::getCurrentProfile()
{
    return airVPNUserCurrentProfile;
}

std::string AirVPNUser::getFirstProfileName()
{
    std::string name = "";

    if(userKey.size() > 0)
    {
        std::map<std::string, UserKey>::iterator it = userKey.begin();
        
        name = it->first;
    }

    return name;
}

void AirVPNUser::setCurrentProfile(std::string p)
{
    airVPNUserCurrentProfile = p;
}

AirVPNUser::Error AirVPNUser::getUserProfileError()
{
    return userProfileError;
}

std::string AirVPNUser::getUserProfileErrorDescription()
{
    return userProfileErrorDescription;
}

AirVPNUser::UserLocationStatus AirVPNUser::detectUserLocation()
{
    UserLocationStatus result;
    std::map<std::string, std::string> userLocation;
    std::map<std::string, std::string>::iterator it;

    userLocation = airVpnTools->detectLocation();

    if(userLocation.size() > 0)
    {
        it = userLocation.find("status");
        
        if(it->second == AirVPNTools::LOCATION_STATUS_OK)
        {
            userLocationStatusError = it->second;

            it = userLocation.find("ip");
            userIP = it->second;

            it = userLocation.find("country");
            userCountry = it->second;

            it = userLocation.find("latitude");
            
            try
            {
                userLatitude = std::stod(it->second);
            }
            catch(std::exception &e)
            {
                userLatitude = 0.0;
            }

            it = userLocation.find("longitude");

            try
            {
                userLongitude = std::stod(it->second);
            }
            catch(std::exception &e)
            {
                userLongitude = 0.0;
            }

            result = SET;
        }
        else
        {
            userIP = "";
            userCountry = "";
            userLatitude = 0.0;
            userLongitude = 0.0;

            userLocationStatusError = "ERROR: Cannot detect user location: " + it->second;

            result = UNKNOWN;
        }
    }
    else
    {
        userIP = "";
        userCountry = "";
        userLatitude = 0.0;
        userLongitude = 0.0;
        
        userLocationStatusError = "ipleak.net returned an empty document";

        result = UNKNOWN;
    }

    return result;
}

void AirVPNUser::loadUserProfile()
{
    airVpnUserProfileDocument = nullptr;
    std::ostringstream osstring;
    std::vector<std::string> bootstrapServerList = AirVPNManifest::getBootStrapServerUrlList();
    std::string rsaPublicKeyModulus = AirVPNManifest::getRsaPublicKeyModulus();
    std::string rsaPublicKeyExponent = AirVPNManifest::getRsaPublicKeyExponent();

    if(airVPNUserName.empty())
    {
        userProfileError = AirVPNUser::Error::INVALID_USER;
        userProfileErrorDescription = "AirVPN Username is empty";
        userProfileType = UserProfileType::NOT_SET;
        userIsValid = false;
        
        return;
    }

    if(airVPNUserPassword.empty())
    {
        userProfileError = AirVPNUser::Error::INVALID_USER;
        userProfileErrorDescription = "AirVPN User password is empty";
        userProfileType = UserProfileType::NOT_SET;
        userIsValid = false;
        
        return;
    }

    if(rsaPublicKeyModulus.empty() || rsaPublicKeyExponent.empty() || bootstrapServerList.empty())
    {
        userProfileError = AirVPNUser::Error::AIRVPN_PARAMETERS_ERROR;
        userProfileErrorDescription = "Invalid AirVPN bootstrap server parameters";
        userProfileType = UserProfileType::NOT_SET;
        userIsValid = false;
        
        return;
    }

    airVpnTools->setRSAModulus(rsaPublicKeyModulus);
    airVpnTools->setRSAExponent(rsaPublicKeyExponent);

    airVpnTools->resetBootServers();

    for(std::string server : bootstrapServerList)
        airVpnTools->addBootServer(server);

    airVpnTools->resetAirVPNDocumentRequest();
    airVpnTools->setAirVPNDocumentParameter("login", airVPNUserName);
    airVpnTools->setAirVPNDocumentParameter("password", airVPNUserPassword);

#if defined(__linux__)
    airVpnTools->setAirVPNDocumentParameter("system", "linux");
#elif __APPLE__
    airVpnTools->setAirVPNDocumentParameter("system", "osx");
#endif

    airVpnTools->setAirVPNDocumentParameter("version", AirVPNManifest::manifestVersion);
    airVpnTools->setAirVPNDocumentParameter("act", "user");

    AirVPNTools::AirVPNServerError res = airVpnTools->requestAirVPNDocument();

    if(res == AirVPNTools::AirVPNServerError::OK)
    {
        userProfileDocument = airVpnTools->getRequestedDocument();
        AirVPNUser::Error error = processXmlUserProfile();

        if(error == AirVPNUser::Error::OK)
        {
            userProfileError = AirVPNUser::Error::OK;
            userProfileErrorDescription = "OK";
            userProfileType = UserProfileType::FROM_SERVER;
        }
        else
        {
            userProfileError = AirVPNUser::Error::PARSE_ERROR;
            userProfileErrorDescription = "";

            switch(error)
            {
                case INVALID_USER:
                {
                    userProfileErrorDescription += "Invalid username or password";
                }
                break;
                
                case CANNOT_RETRIEVE_PROFILE:
                {
                    userProfileErrorDescription += "Cannot retrieve user profile from server";
                }
                break;
                
                case EMPTY_PROFILE:
                {
                    userProfileErrorDescription += "Server returned an empty user profile";
                }
                break;
                
                case NULL_XML_DOCUMENT:
                {
                    userProfileErrorDescription += "Server returned an unknown user profile";
                }
                break;
                
                case PARSE_ERROR:
                {
                    userProfileErrorDescription += "Failed to parse. Malformed user profile.";
                }
                break;
                
                default:
                {
                    userProfileErrorDescription += "Failed to parse. Unknown error.";
                }
                break;
            }

            userProfileDocument = "";
            userProfileType = UserProfileType::NOT_SET;
            userIsValid = false;
        }
    }
    else
    {
        userProfileError = AirVPNUser::Error::INVALID_USER;
        userProfileErrorDescription = airVpnTools->getDocumentRequestErrorDescription();
        userProfileType = UserProfileType::NOT_SET;
        userIsValid = false;
    }
}

AirVPNUser::Error AirVPNUser::processXmlUserProfile()
{
    std::string value;
    xmlNode *node = nullptr;
    
    if(userProfileDocument.empty())
        return EMPTY_PROFILE;

    cleanupXmlParser();

    airVpnUserProfileDocument = xmlReadMemory(userProfileDocument.c_str(), userProfileDocument.size(), "noname.xml", NULL, 0);

    if(airVpnUserProfileDocument == nullptr)
        return NULL_XML_DOCUMENT;

    rootNode = xmlDocGetRootElement(airVpnUserProfileDocument);

    initializeUserProfileData();

    node = AirVPNTools::findXmlNode((xmlChar *)"user", rootNode);

    if(node != NULL)
    {
        if(AirVPNTools::getXmlAttribute(node, "message_action", "").empty())
            userIsValid = true;
        else
        {
            userIsValid = false;

            userProfileErrorDescription = AirVPNTools::getXmlAttribute(node, "message_action", "");
            
            return INVALID_USER;
        }

        if(AirVPNTools::getXmlAttribute(node, "login", "") != airVPNUserName)
        {
            userProfileErrorDescription = "ERROR: User name in profile data does not match to current user";
            
            userIsValid = false;

            return INVALID_USER;
        }
        
        expirationDate = AirVPNTools::getXmlAttribute(node, "expirationdate", "");
        certificateAuthorityCertificate = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "ca", ""));
        tlsAuthKey = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "ta", ""));
        tlsCryptKey = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "tls_crypt", ""));
        sshKey = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "ssh_key", ""));
        sshPpk = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "ssh_ppk", ""));
        sslCertificate = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "ssl_crt", ""));
    }

    if(AirVPNTools::findXmlNode((xmlChar *)"keys", rootNode) != NULL)
    {
        node = rootNode;

        while(node != NULL)
        {
            node = AirVPNTools::findXmlNode((xmlChar *)"key", node);

            if(node != NULL)
            {
                value = AirVPNTools::getXmlAttribute(node, "name", "");
                if(!value.empty())
                {
                    UserKey localUserKey;
                    
                    localUserKey.name = value;
                    localUserKey.certificate = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "crt", ""));
                    localUserKey.privateKey = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "key", ""));
                    
                    addUserKey(value, localUserKey);
                }
                
                node = node->next;
            }
        }
    }
    
    return OK;
}

std::string AirVPNUser::getOpenVPNProfile(std::string profileName, std::string server, int port, std::string proto, std::string tlsMode, std::string cipher, bool connectIPv6, bool mode6to4, bool createCountryProfile, std::string countryCode, std::string customProfile)
{
    std::ostringstream openVpnProfile;

    if(customProfile.empty() && (profileName.empty() || userKey.find(profileName) == userKey.end()))
    {
        global_log("ERROR: AirVPNUser: Wrong profile name " + profileName);

        return "";
    }

    if(server.empty())
    {
        global_log("ERROR: AirVPNUser: server is empty");

        return "";
    }

    if(port < 1 || port > 65535)
    {
        global_log("ERROR: AirVPNUser: illegal port number " + std::to_string(port));

        return "";
    }

    proto = AirVPNTools::toLower(proto);

    if(proto != "udp" && proto != "tcp" && proto != "udp6" && proto != "tcp6")
    {
        global_log("ERROR: AirVPNUser: illegal prptocol " + proto);

        return "";

    }

    tlsMode = AirVPNTools::toLower(tlsMode);

    if(tlsMode != "tls-auth" && tlsMode != "tls-crypt")
    {
        global_log("ERROR: AirVPNUser: illegal tls mode " + tlsMode);

        return "";
    }

    if(createCountryProfile)
    {
        server = AirVPNTools::toLower(countryCode);

        if(tlsMode == "tls-crypt")
            server += "3";

        if(connectIPv6)
            server += ".ipv6";

        server += ".vpn.airdns.org";
    }

    openVpnProfile.str("");

    openVpnProfile << "client" << std::endl << "dev tun" << std::endl;

    openVpnProfile << "remote " << server << " " << port << std::endl;

    openVpnProfile << "proto " << proto << std::endl;

    openVpnProfile << "push-peer-info" << std::endl;

    openVpnProfile << "setenv UV_IPV6 ";

    if(mode6to4 == true || connectIPv6 == true)
        openVpnProfile << "yes";
    else
        openVpnProfile << "no";

    openVpnProfile << std::endl;

    openVpnProfile << "resolv-retry infinite" << std::endl;
    openVpnProfile << "nobind" << std::endl;
    openVpnProfile << "persist-key" << std::endl;
    openVpnProfile << "persist-tun" << std::endl;
    openVpnProfile << "auth-nocache" << std::endl;
    openVpnProfile << "verb 3" << std::endl;

    if(proto == "udp")
        openVpnProfile << "explicit-exit-notify 5" << std::endl;

    openVpnProfile << "remote-cert-tls server" << std::endl;

    if(cipher == "SERVER")
        openVpnProfile << "cipher AES-256-GCM" << std::endl;
    else
    {
        openVpnProfile << "cipher " << cipher << std::endl;
        openVpnProfile << "ncp-disable" << std::endl;
    }

    openVpnProfile << "comp-lzo no" << std::endl;

    if(tlsMode == "tls-crypt")
        openVpnProfile << "auth SHA512" << std::endl;

    if(tlsMode == "tls-auth")
        openVpnProfile << "key-direction 1" << std::endl;

    if(certificateAuthorityCertificate.empty())
    {
        global_log("ERROR: AirVPNUser: Certificate authority certificate (ca) is empty");

        return "";
    }

    if(customProfile.empty())
    {
        openVpnProfile << "<ca>" << std::endl << certificateAuthorityCertificate << std::endl << "</ca>" << std::endl;

        std::map<std::string, UserKey>::iterator it = userKey.find(profileName);
        
        if(it != userKey.end())
        {
            UserKey profile = it->second;

            openVpnProfile << "<cert>" << std::endl << profile.certificate << std::endl << "</cert>" << std::endl;

            openVpnProfile << "<key>" << std::endl << profile.privateKey << std::endl << "</key>" << std::endl;
        }

        if(tlsMode == "tls-auth")
            openVpnProfile << "<tls-auth>" << std::endl << tlsAuthKey << std::endl << "</tls-auth>" << std::endl;
        else if(tlsMode == "tls-crypt")
            openVpnProfile << "<tls-crypt>" << std::endl << tlsCryptKey << std::endl << "</tls-crypt>" << std::endl;
    }
    else
        openVpnProfile << std::endl << customProfile;

    return openVpnProfile.str();
}

std::string AirVPNUser::getUserLocationStatusError()
{
    return userLocationStatusError;
}

void AirVPNUser::cleanupXmlParser()
{
    if(airVpnUserProfileDocument == nullptr)
        return;

    xmlFreeDoc(airVpnUserProfileDocument);
}

size_t AirVPNUser::curlWriteCallback(void *data, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)data, size * nmemb);

    return size * nmemb;
}
