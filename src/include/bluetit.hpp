/*
 * bluetit.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2021 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef BLUETIT_HPP
#define BLUETIT_HPP

#include "dbusconnector.hpp"
#include "rcparser.hpp"
#include "optionparser.hpp"

#include <future>

#define BLUETIT_FULL_NAME           BLUETIT_NAME " " BLUETIT_VERSION
#define BLUETIT_RESOURCE_DIRECTORY  "/etc/airvpn"
#define BLUETIT_LOCK_FILE           BLUETIT_RESOURCE_DIRECTORY "/bluetit.lock"
#define BLUETIT_RC_FILE             BLUETIT_RESOURCE_DIRECTORY "/bluetit.rc"
#define HUMMINGBIRD_LOCK_FILE       BLUETIT_RESOURCE_DIRECTORY "/hummingbird.lock"
#define RESOLVDOTCONF_BACKUP        BLUETIT_RESOURCE_DIRECTORY "/resolv.conf.airvpnbackup"
#define SYSTEM_DNS_BACKUP_FILE      BLUETIT_RESOURCE_DIRECTORY "/systemdns.airvpnbackup"
#define CONNECTION_SEQUENCE_FILE    BLUETIT_RESOURCE_DIRECTORY "/connection_sequence.csv"
#define BLUETIT_LOG_NAME            "bluetit"

#define TLS_MODE_CRYPT                      "tls-crypt"
#define TLS_MODE_AUTH                       "tls-auth"

#define DEFAULT_PROTO                       "udp"
#define DEFAULT_PORT                        "443"
#define DEFAULT_TLS_MODE                    TLS_MODE_CRYPT
#define DEFAULT_IPV6_MODE                   false
#define DEFAULT_6TO4_MODE                   false

#define DEFAULT_MANIFEST_UPDATE_INTERVAL    15
#define MANIFEST_THREAD_SLEEP_SECONDS       5
#define DEFAULT_MAX_CONNECTION_RETRIES      10

#define CONNECTION_STATS_INTERVAL           2

#define WAIT_AIRVPN_MANIFEST_TIMEOUT        20

#define WAIT_NETWORK_INTERVAL                2
#define WAIT_NETWORK_MESSAGE_INTERVAL        60

// Common return messages

#define MESSAGE_AIRVPN_NOT_AVAILABLE            "ERROR: AirVPN services and connection are disabled"
#define MESSAGE_AIRVPN_SERVER_NOT_FOUND         "ERROR: AirVPN server not found"
#define MESSAGE_AIRVPN_COUNTRY_NOT_FOUND        "ERROR: AirVPN country not found"
#define MESSAGE_AIRVPN_USER_PROFILE_NOT_FOUND   "ERROR: AirVPN user profile not found"

// Run Control directives

#define RC_DIRECTIVE_BOOTSERVER                 "bootserver"
#define RC_DIRECTIVE_RSAEXPONENT                "rsaexponent"
#define RC_DIRECTIVE_RSAMODULUS                 "rsamodulus"
#define RC_DIRECTIVE_MANIFESTUPDATEINTERVAL     "manifestupdateinterval"
#define RC_DIRECTIVE_AIRCONNECTATBOOT           "airconnectatboot"
#define RC_DIRECTIVE_NETWORKLOCKPERSIST         "networklockpersist"
#define RC_DIRECTIVE_AIRUSERNAME                "airusername"
#define RC_DIRECTIVE_AIRPASSWORD                "airpassword"
#define RC_DIRECTIVE_AIRKEY                     "airkey"
#define RC_DIRECTIVE_AIRSERVER                  "airserver"
#define RC_DIRECTIVE_AIRCOUNTRY                 "aircountry"
#define RC_DIRECTIVE_AIRPORT                    "airport"
#define RC_DIRECTIVE_AIRPROTO                   "airproto"
#define RC_DIRECTIVE_AIRCIPHER                  "aircipher"
#define RC_DIRECTIVE_AIRIPV6                    "airipv6"
#define RC_DIRECTIVE_AIR6TO4                    "air6to4"
#define RC_DIRECTIVE_AIRWHITESERVERLIST         "airwhiteserverlist"
#define RC_DIRECTIVE_AIRBLACKSERVERLIST         "airblackserverlist"
#define RC_DIRECTIVE_AIRWHITECOUNTRYLIST        "airwhitecountrylist"
#define RC_DIRECTIVE_AIRBLACKCOUNTRYLIST        "airblackcountrylist"
#define RC_DIRECTIVE_COUNTRY                    "country"
#define RC_DIRECTIVE_REMOTE                     "remote"
#define RC_DIRECTIVE_PROTO                      "proto"
#define RC_DIRECTIVE_PORT                       "port"
#define RC_DIRECTIVE_TUNPERSIST                 "tunpersist"
#define RC_DIRECTIVE_CIPHER                     "cipher"
#define RC_DIRECTIVE_MAXCONNECTIONRETRIES       "maxconnretries"
#define RC_DIRECTIVE_TCPQUEUELIMIT              "tcpqueuelimit"
#define RC_DIRECTIVE_NCPDISABLE                 "ncpdisable"
#define RC_DIRECTIVE_NETWORKLOCK                "networklock"
#define RC_DIRECTIVE_IGNOREDNSPUSH              "ignorednspush"
#define RC_DIRECTIVE_TIMEOUT                    "timeout"
#define RC_DIRECTIVE_COMPRESS                   "compress"
#define RC_DIRECTIVE_TLSVERSIONMIN              "tlsversionmin"
#define RC_DIRECTIVE_PROXYHOST                  "proxyhost"
#define RC_DIRECTIVE_PROXYPORT                  "proxyport"
#define RC_DIRECTIVE_PROXYUSERNAME              "proxyusername"
#define RC_DIRECTIVE_PROXYPASSWORD              "proxypassword"
#define RC_DIRECTIVE_PROXYBASIC                 "proxybasic"

#define BLUETIT_RC_TEMPLATE       "#\n" \
                                  "# bluetit runcontrol file\n" \
                                  "#\n" \
                                  "\n" \
                                  "# " RC_DIRECTIVE_BOOTSERVER "\t\t\t\t<ip|url>\n" \
                                  "# " RC_DIRECTIVE_RSAEXPONENT "\t\t\t\t<value>\n" \
                                  "# " RC_DIRECTIVE_RSAMODULUS "\t\t\t\t<value>\n" \
                                  "# " RC_DIRECTIVE_AIRCONNECTATBOOT "\t\t\t<off|quick|server|country>\n" \
                                  "# " RC_DIRECTIVE_NETWORKLOCKPERSIST "\t\t<on|iptables|nftables|pf|off>\n" \
                                  "# " RC_DIRECTIVE_AIRUSERNAME "\t\t\t\t<airvpn_username>\n" \
                                  "# " RC_DIRECTIVE_AIRPASSWORD "\t\t\t\t<aivpn_password>\n" \
                                  "# " RC_DIRECTIVE_AIRKEY "\t\t\t\t\t<airvpn_user_key>\n" \
                                  "# " RC_DIRECTIVE_AIRSERVER "\t\t\t\t\t<airvpn_server_name>\n" \
                                  "# " RC_DIRECTIVE_AIRCOUNTRY "\t\t\t\t<airvpn_country_name>\n" \
                                  "# " RC_DIRECTIVE_AIRPROTO "\t\t\t\t\t<udp|tcp>\n" \
                                  "# " RC_DIRECTIVE_AIRPORT "\t\t\t\t\t<port>\n" \
                                  "# " RC_DIRECTIVE_AIRCIPHER "\t\t\t\t\t<cipher_name>\n" \
                                  "# " RC_DIRECTIVE_AIRIPV6 "\t\t\t\t\t<yes|no>\n" \
                                  "# " RC_DIRECTIVE_AIR6TO4 "\t\t\t\t\t<yes|no>\n" \
                                  "# " RC_DIRECTIVE_MANIFESTUPDATEINTERVAL "\t<minutes>\n" \
                                  "# " RC_DIRECTIVE_AIRWHITESERVERLIST "\t\t<server list>\n" \
                                  "# " RC_DIRECTIVE_AIRBLACKSERVERLIST "\t\t<server list>\n" \
                                  "# " RC_DIRECTIVE_AIRWHITECOUNTRYLIST "\t\t<server list>\n" \
                                  "# " RC_DIRECTIVE_AIRBLACKCOUNTRYLIST "\t\t<server list>\n" \
                                  "# " RC_DIRECTIVE_COUNTRY "\t\t\t\t\t<ISO code>\n" \
                                  "# " RC_DIRECTIVE_REMOTE "\t\t\t\t\t<ip|url list>\n" \
                                  "# " RC_DIRECTIVE_PROTO "\t\t\t\t\t\t<udp|tcp>\n" \
                                  "# " RC_DIRECTIVE_PORT "\t\t\t\t\t\t<port>\n" \
                                  "# " RC_DIRECTIVE_TUNPERSIST "\t\t\t\t<yes|no>\n" \
                                  "# " RC_DIRECTIVE_CIPHER "\t\t\t\t\t<cipher_names>\n" \
                                  "# " RC_DIRECTIVE_MAXCONNECTIONRETRIES "\t\t\t<number>\n" \
                                  "# " RC_DIRECTIVE_TCPQUEUELIMIT "\t\t\t\t<value>\n" \
                                  "# " RC_DIRECTIVE_NCPDISABLE "\t\t\t\t<yes|no>\n" \
                                  "# " RC_DIRECTIVE_NETWORKLOCK "\t\t\t\t<on|iptables|nftables|pf|off>\n" \
                                  "# " RC_DIRECTIVE_IGNOREDNSPUSH "\t\t\t\t<yes|no>\n" \
                                  "# " RC_DIRECTIVE_TIMEOUT "\t\t\t\t\t<seconds>\n" \
                                  "# " RC_DIRECTIVE_COMPRESS "\t\t\t\t\t<yes|no|asym>\n" \
                                  "# " RC_DIRECTIVE_TLSVERSIONMIN "\t\t\t\t<disabled|default|tls_1_x>\n" \
                                  "# " RC_DIRECTIVE_PROXYHOST "\t\t\t\t\t<ip|url>\n" \
                                  "# " RC_DIRECTIVE_PROXYPORT "\t\t\t\t\t<port>\n" \
                                  "# " RC_DIRECTIVE_PROXYUSERNAME "\t\t\t\t<username>\n" \
                                  "# " RC_DIRECTIVE_PROXYPASSWORD "\t\t\t\t<password>\n" \
                                  "# " RC_DIRECTIVE_PROXYBASIC "\t\t\t\t<yes|no>\n"

#define NETLOCKMODE_PERSISTENT  1
#define NETLOCKMODE_SESSION     2
                                  
struct ConnectionScheme
{
    std::string protocol;
    int port;
    int entry;
};
    
void signal_handler(int signum);
void create_daemon();
bool check_if_root();
void global_log(std::string s);
void global_syslog(std::string s);
int bluetit_status(void);
std::string bluetit_status_description(void);
void cleanup_and_exit(int exit_code);
std::string recover_network();
bool setup_rc_directives();
bool setup_options();
void reset_settings();
bool enable_network_lock(int mode);
bool disable_network_lock();
bool add_airvpn_bootstrap_to_network_lock();
bool remove_airvpn_bootstrap_from_network_lock();
std::string check_rc_value(std::string description, std::string option, std::string value);
std::string denied_rc_value(std::string description, std::string option, std::string value);
std::string set_openvpn_client_config();
std::string set_openvpn_profile(std::string profile);
std::string set_bluetit_options(std::vector<std::string> option);
void start_boot_airvpn_connection();
void start_airvpn_connection();
void start_airvpn_quick_connection(std::future<void> future);
void establish_openvpn_connection();
void start_openvpn_connection(int max_connection_retries);
std::string stop_openvpn_connection();
std::string reconnect_openvpn();
std::string pause_openvpn_connection();
std::string resume_openvpn_connection();
void connection_stats_updater(std::future<void> future);
void connection_stats(DBusResponse &response);
void airvpn_server_info(std::string name, DBusResponse &response);
void airvpn_server_list(std::string pattern, DBusResponse &response);
void airvpn_country_info(std::string name, DBusResponse &response);
void airvpn_country_list(std::string pattern, DBusResponse &response);
void airvpn_key_list(std::string username, std::string password, DBusResponse &response);
void airvpn_key_save(std::string username, std::string password, DBusResponse &response);
void airvpn_server_save(std::string username, std::string password, bool create_country, DBusResponse &response);
std::string airvpn_create_profile(bool create_country_profile, int ipEntryOffset = 0);
void airvpn_manifest_updater(int interval_minutes, std::future<void> future);
bool airvpn_user_login();
void airvpn_user_logout();
void terminate_client_session();
void send_event(std::string event, std::string payload);

#endif
