/*
 * airvpnserverprovider.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2021 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef AIRVPNSERVERPROVIDER_CLASS_HPP
#define AIRVPNSERVERPROVIDER_CLASS_HPP

#include <string>
#include <map>
#include <vector>

#include "airvpnmanifest.hpp"
#include "airvpnserver.hpp"
#include "airvpnuser.hpp"
#include "countrycontinent.hpp"

class AirVPNServerProvider
{
    public:

    enum TLSMode
    {
        NOT_SET,
        TLS_AUTH,
        TLS_CRYPT
    };

    AirVPNServerProvider(AirVPNUser *user, std::string resourceDirectory);
    ~AirVPNServerProvider();
    
    std::string getUserIP();
    void setUserIP(std::string u);
    std::string getUserCountry();
    void setUserCountry(std::string c);
    TLSMode getTlsMode();
    void setTlsMode(TLSMode m);
    bool getSupportIPv4();
    void setSupportIPv4(bool s);
    bool getSupportIPv6();
    void setSupportIPv6(bool s);
    std::vector<std::string> getServerWhitelist();
    void setServerWhitelist(std::vector<std::string> list);
    std::vector<std::string> getServerBlacklist();
    void setServerBlacklist(std::vector<std::string> list);
    std::vector<std::string> getCountryWhitelist();
    void setCountryWhitelist(std::vector<std::string> list);
    std::vector<std::string> getCountryBlacklist();
    void setCountryBlacklist(std::vector<std::string> list);

    std::vector<AirVPNServer> getFilteredServerList();

    private:

    std::string AIRVPN_CONNECTION_PRIORITY_FILE_NAME = "connection_priority.txt";

    AirVPNManifest *airVPNManifest = nullptr;
    AirVPNUser *airVPNUser = nullptr;

    std::string userIP = "";
    std::string userCountry = "";
    std::string userContinent = "";
    TLSMode tlsMode;
    bool supportIPv4 = false;
    bool supportIPv6 = false;

    std::vector<std::string> serverWhitelist, serverBlacklist;
    std::vector<std::string> countryWhitelist, countryBlacklist;

    CountryContinent *countryContinent = nullptr;

    std::map<std::string, std::string> connectionPriority;

    static bool compareServerScore(AirVPNServer s1, AirVPNServer s2);

    void loadConnectionPriorities(std::string resourceDirectory);

    std::vector<std::string> getUserConnectionPriority();
};

#endif
