/*
 * optionparser.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2021 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef OPTIONPARSER_CLASS_HPP
#define OPTIONPARSER_CLASS_HPP

#include <vector>
#include <string>

class OptionParser
{
    public:

    enum Error
    {
        OK,
        NO_OPTIONS_PROVIDED,
        UNKNOWN_OPTION,
        VALUE_REQUIRED,
        INVALID_VALUE,
        SYNTAX_ERROR,
        PARSE_ERROR
    };

    enum Type
    {
        UNDEFINED,
        OPTION,
        INTEGER,
        NUMBER,
        STRING,
        BOOL
    };

    struct OptionConfig
    {
        std::string shortName;
        std::string longName;
        Type type;
    };

    struct Option
    {
        std::string shortName;
        std::string longName;
        bool isValid;
        Type type;
        std::string value;
        std::string error;
    };

    typedef std::vector<Option *> Options;

    OptionParser();
    ~OptionParser();

    Error parseOptions(std::vector<std::string> opt);
    Options getOptions();
    Options getInvalidOptions();
    Option *getOption(std::string name);
    bool addConfigOption(std::string sname, std::string lname, Type type);
    bool isOptionEnabled(Option *opt);
    std::string getErrorDescription();

    static bool isValidBool(std::string value);
    static bool isBoolEnabled(std::string value);
    static std::string allowedBoolValueMessage(std::string opt);

    private:

    std::vector<OptionConfig *> configOption;
    Options option;
    std::string errorDescription;

    void setup();
    OptionConfig getConfigOption(std::string name);
    Options optionVector(bool valid);
};

#endif

