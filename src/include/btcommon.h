/*
 * btcommon.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2021 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef BTCOMMON_HPP
#define BTCOMMON_HPP

// Bluetit common descriptions

#define BLUETIT_NAME            "Bluetit - AirVPN OpenVPN 3 Service"
#define BLUETIT_SHORT_NAME      "Bluetit"
#define BLUETIT_VERSION         "1.1.0"
#define BLUETIT_RELEASE_DATE    "4 June 2021"

// Status

#define BLUETIT_STATUS_READY                     1
#define BLUETIT_STATUS_CONNECTED                 2
#define BLUETIT_STATUS_PAUSED                    3
#define BLUETIT_STATUS_DIRTY_EXIT                4
#define BLUETIT_STATUS_RESOURCE_DIRECTORY_ERROR  5
#define BLUETIT_STATUS_INIT_ERROR                6
#define BLUETIT_STATUS_LOCK_ERROR                7
#define BLUETIT_STATUS_UNKNOWN                   99

// D-Bus

#define BT_DBUS_INTERFACE_NAME      "org.airvpn.dbus"
#define BT_SERVER_BUS_NAME          "org.airvpn.server"
#define BT_CLIENT_BUS_NAME          "org.airvpn.client"
#define BT_SERVER_OBJECT_PATH_NAME  "/org/airvpn/server"
#define BT_CLIENT_OBJECT_PATH_NAME  "/org/airvpn/client"

// Commands / Methods

#define BT_METHOD_VERSION                   "version"
#define BT_METHOD_BLUETIT_STATUS            "bluetit_status"
#define BT_METHOD_RESET_BLUETIT_OPTIONS     "reset_bluetit_options"
#define BT_METHOD_AIRVPN_SERVER_INFO        "airvpn_server_info"
#define BT_METHOD_AIRVPN_SERVER_LIST        "airvpn_server_list"
#define BT_METHOD_AIRVPN_COUNTRY_INFO       "airvpn_country_info"
#define BT_METHOD_AIRVPN_COUNTRY_LIST       "airvpn_country_list"
#define BT_METHOD_AIRVPN_KEY_LIST           "airvpn_key_list"
#define BT_METHOD_AIRVPN_SET_KEY            "airvpn_set_key"
#define BT_METHOD_AIRVPN_SAVE               "airvpn_save"
#define BT_METHOD_AIRVPN_START_CONNECTION   "airvpn_start_connection"
#define BT_METHOD_RECOVER_NETWORK           "recover_network"
#define BT_METHOD_OPENVPN_INFO              "openvpn_info"
#define BT_METHOD_OPENVPN_COPYRIGHT         "openvpn_copyright"
#define BT_METHOD_SET_OPTIONS               "set_options"
#define BT_METHOD_GET_OPTION                "get_option"
#define BT_METHOD_SET_OPENVPN_PROFILE       "set_openvpn_profile"
#define BT_METHOD_START_CONNECTION          "start_connection"
#define BT_METHOD_STOP_CONNECTION           "stop_connection"
#define BT_METHOD_PAUSE_CONNECTION          "pause_connection"
#define BT_METHOD_RESUME_CONNECTION         "resume_connection"
#define BT_METHOD_RECONNECT_CONNECTION      "reconnect_connection"
#define BT_METHOD_SESSION_PAUSE             "session_pause"
#define BT_METHOD_SESSION_RESUME            "session_resume"
#define BT_METHOD_SESSION_RECONNECT         "session_reconnect"
#define BT_METHOD_CONNECTION_STATS          "connection_stats"
#define BT_METHOD_ENABLE_NETWORK_LOCK       "enable_network_lock"
#define BT_METHOD_DISABLE_NETWORK_LOCK      "disable_network_lock"
#define BT_METHOD_NETWORK_LOCK_STATUS       "network_lock_status"
#define BT_METHOD_EVENT                     "event"
#define BT_METHOD_LOG                       "log"

// Events

#define BT_EVENT_END_OF_SESSION             "event_end_of_session"
#define BT_EVENT_CONNECTED                  "event_connected"
#define BT_EVENT_DISCONNECTED               "event_disconnected"
#define BT_EVENT_PAUSE                      "event_pause"
#define BT_EVENT_RESUME                     "event_resume"
#define BT_EVENT_ERROR                      "event_error"

// Return codes

#define BT_OK                           "OK"
#define BT_ERROR                        "ERROR"

#define TCP_QUEUE_LIMIT_DEFAULT         8192

#endif
