/*
 * goldcrest.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2021 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef GOLDCREST_HPP
#define GOLDCREST_HPP

#include "btcommon.h"
#include "dbusconnector.hpp"
#include "rcparser.hpp"

#define GOLDCREST_NAME              "Goldcrest"
#define GOLDCREST_VERSION           "1.1.0"
#define GOLDCREST_RELEASE_DATE      "4 June 2021"
#define GOLDCREST_FULL_NAME         GOLDCREST_NAME " " GOLDCREST_VERSION
#define XDG_CONFIG_DIR              "/.config"
#define GOLDCREST_XDG_CONFIG_FILE   XDG_CONFIG_DIR "/goldcrest.rc"
#define GOLDCREST_RC_FILE           "/.goldcrest.rc"

#define RC_DIRECTIVE_AIRVPN_SERVER          "air-server"
#define RC_DIRECTIVE_AIRVPN_TLS_MODE        "air-tls-mode"
#define RC_DIRECTIVE_AIRVPN_IPV6            "air-ipv6"
#define RC_DIRECTIVE_AIRVPN_6TO4            "air-6to4"
#define RC_DIRECTIVE_AIRVPN_USERNAME        "air-user"
#define RC_DIRECTIVE_AIRVPN_PASSWORD        "air-password"
#define RC_DIRECTIVE_AIRVPN_KEY             "air-key"
#define RC_DIRECTIVE_AIRWHITESERVERLIST     "air-white-server-list"
#define RC_DIRECTIVE_AIRBLACKSERVERLIST     "air-black-server-list"
#define RC_DIRECTIVE_AIRWHITECOUNTRYLIST    "air-white-country-list"
#define RC_DIRECTIVE_AIRBLACKCOUNTRYLIST    "air-black-country-list"
#define RC_DIRECTIVE_GC_CIPHER              "cipher"
#define RC_DIRECTIVE_GC_PROTO               "proto"
#define RC_DIRECTIVE_GC_SERVER              "server"
#define RC_DIRECTIVE_GC_PORT                "port"
#define RC_DIRECTIVE_GC_TCP_QUEUE_LIMIT     "tcp-queue-limit"
#define RC_DIRECTIVE_GC_NCP_DISABLE         "ncp-disable"
#define RC_DIRECTIVE_GC_NETWORK_LOCK        "network-lock"
#define RC_DIRECTIVE_GC_IGNORE_DNS_PUSH     "ignore-dns-push"
#define RC_DIRECTIVE_GC_IPV6                "ipv6"
#define RC_DIRECTIVE_GC_TIMEOUT             "timeout"
#define RC_DIRECTIVE_GC_COMPRESS            "compress"
#define RC_DIRECTIVE_GC_PROXY_HOST          "proxy-host"
#define RC_DIRECTIVE_GC_PROXY_PORT          "proxy-port"
#define RC_DIRECTIVE_GC_PROXY_USERNAME      "proxy-username"
#define RC_DIRECTIVE_GC_PROXY_PASSWORD      "proxy-password"
#define RC_DIRECTIVE_GC_PROXY_BASIC         "proxy-basic"
#define RC_DIRECTIVE_GC_ALT_PROXY           "alt-proxy"
#define RC_DIRECTIVE_GC_PERSIST_TUN         "persist-tun"
#define RC_DIRECTIVE_GC_CONN_STAT_SECS      "conn-stat-interval"

#define GOLDCREST_RC_TEMPLATE       "#\n" \
                                    "# goldcrest runcontrol file\n" \
                                    "#\n" \
                                    "\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_SERVER "\t\t<server_name>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_TLS_MODE "\t\t<auto|auth|crypt>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_IPV6 "\t\t<on|off>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_6TO4 "\t\t<on|off>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_USERNAME "\t\t<username>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_PASSWORD "\t\t<password>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_KEY "\t\t<name>\n" \
                                    "# " RC_DIRECTIVE_GC_CIPHER "\t\t<cipher_name>\n" \
                                    "# " RC_DIRECTIVE_GC_PROTO"\t\t\t<udp|tcp>\n" \
                                    "# " RC_DIRECTIVE_GC_SERVER "\t\t<server_ip|server_url>\n" \
                                    "# " RC_DIRECTIVE_GC_PORT "\t\t\t<port>\n" \
                                    "# " RC_DIRECTIVE_GC_TCP_QUEUE_LIMIT "\t<n>\n" \
                                    "# " RC_DIRECTIVE_GC_NCP_DISABLE "\t\t<yes|no>\n" \
                                    "# " RC_DIRECTIVE_GC_NETWORK_LOCK "\t\t<on|iptables|nftables|pf|off>\n" \
                                    "# " RC_DIRECTIVE_GC_IGNORE_DNS_PUSH "\t<yes|no>\n" \
                                    "# " RC_DIRECTIVE_GC_IPV6 "\t\t\t<yes|no|default>\n" \
                                    "# " RC_DIRECTIVE_GC_TIMEOUT "\t\t<seconds>\n" \
                                    "# " RC_DIRECTIVE_GC_COMPRESS "\t\t<yes|no|asym>\n" \
                                    "# " RC_DIRECTIVE_GC_PROXY_HOST "\t\t<host_ip|host_url>\n" \
                                    "# " RC_DIRECTIVE_GC_PROXY_PORT "\t\t<port>\n" \
                                    "# " RC_DIRECTIVE_GC_PROXY_USERNAME "\t<proxy_username>\n" \
                                    "# " RC_DIRECTIVE_GC_PROXY_PASSWORD "\t<proxy_password>\n" \
                                    "# " RC_DIRECTIVE_GC_PROXY_BASIC "\t\t<yes|no>\n" \
                                    "# " RC_DIRECTIVE_GC_ALT_PROXY "\t\t<yes|no>\n" \
                                    "# " RC_DIRECTIVE_GC_PERSIST_TUN "\t\t<on|off>\n" \
                                    "# " RC_DIRECTIVE_GC_CONN_STAT_SECS "\t\t<seconds>\n"

#define DEFAULT_CONN_STAT_SECONDS  60

void signal_handler(int sig);
void cleanup_and_exit(const char *msg, int exit_value);
int get_server_status();
DBusResponse *send_method_to_server(const char *method);
DBusResponse *send_method_with_args_to_server(const char *method, std::vector<std::string> args);
void dbus_message_loop_until(std::string exit_event);
bool check_airvpn_mode(int argc, char **argv);
bool check_airvpn_connect(int argc, char **argv);
bool check_airvpn_key_load(int argc, char **argv);
void check_airvpn_credentials(int argc, char **argv);
bool read_configuration();
void airvpn_server_info(DBusResponse *response);
void airvpn_server_list(DBusResponse *response);
void airvpn_country_info(DBusResponse *response);
void airvpn_country_list(DBusResponse *response);
void airvpn_key_list(DBusResponse *response);
void airvpn_save(DBusResponse *response);
void log(const char *msg);
void usage();
void aboutDevelopmentCredits();
void connection_stats(int interval_seconds, std::future<void> future);
void show_connection_stats();

#endif
