/*
 * hummingbird.cpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2021 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "include/hummingbird.hpp"
#include "include/airvpntools.hpp"
#include "include/vpnclient.hpp"
#include "include/optionparser.hpp"

#include <openvpn/time/timestr.hpp>

bool init_check(void);
bool setup_options();
bool clean_up(void);
void usage(void);
void aboutDevelopmentCredits(void);
void global_log(std::string msg);

// C functions

static VpnClient *vpnClient = nullptr;
OptionParser *optionParser = nullptr;
ClientAPI::Config config;

void show_intro(void)
{
    std::cout << HUMMINGBIRD_FULL_NAME << " - " << HUMMINGBIRD_RELEASE_DATE << std::endl << std::endl;
}

bool check_if_root()
{
    uid_t uid, euid;
    bool retval = false;

    uid=getuid();
    euid=geteuid();

    if(uid != 0 || uid != euid)
        retval = false;
    else
        retval = true;

    return retval;
}

bool init_check(void)
{
    DIR* resdir;
    FILE *flock;
    char buf[256];
    bool dirtyExit = false, lockFileFound = false;

    // Check for resource directory

    resdir = opendir(RESOURCE_DIRECTORY);

    if(resdir)
    {
        closedir(resdir);
    }
    else if(errno == ENOENT)
    {
        std::cout << "Creating resource directory " << RESOURCE_DIRECTORY << std::endl;

        if(mkdir(RESOURCE_DIRECTORY, 0755) != 0)
        {
            std::cout << "Cannot create " << RESOURCE_DIRECTORY << " (Error " << errno << " - " << strerror(errno) << std::endl;

            return false;
        }
    }
    else
    {
        std::cout << "Cannot access resource directory" << RESOURCE_DIRECTORY << std::endl;

        return false;
    }

    NetFilter *netFilter = new NetFilter(RESOURCE_DIRECTORY, NetFilter::Mode::OFF);

    if(netFilter->systemBackupExists() == true)
        dirtyExit = true;

    delete netFilter;

#if defined(OPENVPN_PLATFORM_LINUX)

    DNSManager *dnsManager = new DNSManager(RESOLVDOTCONF_BACKUP);

    if(dnsManager->resolvDotConfBackupExists())
        dirtyExit = true;

    if(access(RESOLVDOTCONF_BACKUP, F_OK) == 0)
        dirtyExit = true;
    
    if(access(SYSTEM_DNS_BACKUP_FILE, F_OK) == 0)
        dirtyExit = true;

    delete dnsManager;

#endif

    if(access(HUMMINGBIRD_LOCK_FILE, F_OK) == 0)
        lockFileFound = true;

    if(lockFileFound == true || dirtyExit == true)
    {
        if(lockFileFound == true)
        {
            std::cout << "This program is already running ";

            flock = fopen(HUMMINGBIRD_LOCK_FILE, "r");

            if(fgets(buf, sizeof(buf), flock) != NULL)
                std::cout << "(PID " << atoi(buf) << ") ";

            std::cout << "or it did not gracefully" << std::endl;

            fclose(flock);

            std::cout << "exit in its previous execution. In case you have restarted this computer" << std::endl;
            std::cout << "or just powered it on, you can remove the lock file " << HUMMINGBIRD_LOCK_FILE << std::endl;
            std::cout << "and start this program again." << std::endl;
            std::cout << "In case you are sure this program is not already running and your network" << std::endl;
            std::cout << "connection is not working as expected, you can run this program with the" << std::endl;
            std::cout << "\"--recover-network\" option in order to try restoring your system network" << std::endl;
            std::cout << "settings." << std::endl;
        }
        else
        {
            std::cout << "It seems this program did not exit gracefully or has been killed." << std::endl;
            std::cout << "Your system may not be working properly and your network connection may not work" << std::endl;
            std::cout << "as expected. To recover your network settings, run this program again and use" << std::endl;
            std::cout << "the \"--recover-network\" option." << std::endl;
        }

        std::cout << std::endl;

        return false;
    }

    // Create lock file

    flock = fopen(HUMMINGBIRD_LOCK_FILE, "w");

    fprintf(flock, "%d\n", getpid());

    fclose(flock);

    return true;
}

bool clean_up(void)
{
    // remove lock file

    if(access(HUMMINGBIRD_LOCK_FILE, F_OK) != -1)
        unlink(HUMMINGBIRD_LOCK_FILE);

    // remove system dns backup file

    if(access(SYSTEM_DNS_BACKUP_FILE, F_OK) != -1)
        unlink(SYSTEM_DNS_BACKUP_FILE);

    if(optionParser != nullptr)
        delete optionParser;

    return true;
}

static void worker_thread()
{
    try
    {
        std::cout << date_time() << " Starting thread" << std::endl;

        ClientAPI::Status connect_status = vpnClient->connect();

        if(connect_status.error)
        {
            std::cout << date_time() << " OpenVPN3 CONNECT ERROR: ";

            if(!connect_status.status.empty())
                std::cout << connect_status.status << ": ";

            std::cout << connect_status.message << std::endl;
        }
    }
    catch(const std::exception& e)
    {
        std::cout << date_time() << " OpenVPN3 Connect thread exception: " << e.what() << std::endl;
    }

    std::cout << date_time() << " Thread finished" << std::endl;
}

static std::string read_profile(const char *fn, const std::string* profile_content)
{
    if(!string::strcasecmp(fn, "http") && profile_content && !profile_content->empty())
        return *profile_content;
    else
    {
        if(access(fn, F_OK) == -1)
            OPENVPN_THROW_EXCEPTION("Profile " << fn << " not found");

        ProfileMerge pm(fn, "", "", ProfileMerge::FOLLOW_FULL, ProfileParseLimits::MAX_LINE_SIZE, ProfileParseLimits::MAX_PROFILE_SIZE);

        if(pm.status() != ProfileMerge::MERGE_SUCCESS)
            OPENVPN_THROW_EXCEPTION("merge config error: " << pm.status_string() << " : " << pm.error());

        return pm.profile_content();
    }
}

static void signal_handler(int signum)
{
    static bool pause_toggle = false;
    std::string signalDescription;

    switch(signum)
    {
        case SIGTERM:
        case SIGINT:
        case SIGPIPE:
        case SIGHUP:
        {
            if(signum == SIGTERM)
                signalDescription = "SIGTERM";
            else if(signum == SIGINT)
                signalDescription = "SIGINT";
            else if(signum == SIGPIPE)
                signalDescription = "SIGPIPE";
            else
                signalDescription = "SIGHUP";

            std::cout << "Received " << signalDescription << " signal. Terminating " << HUMMINGBIRD_SHORT_NAME << "." << std::endl;

            if(vpnClient)
                vpnClient->stop();
        }
        break;

        case SIGUSR1:
        {
            std::cout << "Received SIGUSR1 signal. ";

            // toggle pause/resume

            if(config.tunPersist == false)
            {
                std::cout << "TUN persistence is not enabled. Pause/resume is ignored." << std::endl;

                return;
            }

            if(vpnClient)
            {
                if(pause_toggle)
                {
                    std::cout << "Resuming VPN connection.";

                    vpnClient->resume();
                }
                else
                {
                    std::cout << "Pausing VPN connection.";

                    vpnClient->pause("pause-resume-signal");
                }

                pause_toggle = !pause_toggle;
            }
            else
                std::cout << "VPN is not connected. Signal ignored";

            std::cout << std::endl;
        }
        break;

        case SIGUSR2:
        {
            std::cout << "Received SIGUSR2 signal. ";

            if(config.tunPersist == false)
            {
                std::cout << "TUN persistence is not enabled. Reconnection is ignored." << std::endl;

                return;
            }

            std::cout << "Reconnecting VPN." << std::endl;

            if(vpnClient)
                vpnClient->reconnect(0);
        }
        break;

        default:
        {
            std::cout << "Received unhandled signal " << signum << std::endl;
        }
        break;
    }
}

static void start_connection_thread(VpnClient *client)
{
    std::unique_ptr<std::thread> thread;

    // start connect thread

    vpnClient = client;

    thread.reset(new std::thread([]()
        {
            worker_thread();
        }));

    {
        // catch signals that might occur while we're in join()

        Signal signal(signal_handler, Signal::F_SIGINT|Signal::F_SIGTERM|Signal::F_SIGPIPE|Signal::F_SIGHUP|Signal::F_SIGUSR1|Signal::F_SIGUSR2);

        // wait for connect thread to exit

        thread->join();
    }
}

int openvpn_client(std::vector<std::string> options, const std::string profile_filename)
{
    VpnClient *client = nullptr;
    OptionParser::Error optionError;
    OptionParser::Options parserOptions;
    NetFilter::Mode network_lock_mode = NetFilter::Mode::AUTO;
    int ret = 0, timeout = 0, defaultKeyDirection = -1, sslDebugLevel = 0;
    unsigned int tcp_queue_limit = TCP_QUEUE_LIMIT_DEFAULT;
    bool eval = false, ncp_disable = false, ignore_dns_push = false, cachePassword = false;
    bool disableClientCert = false, proxyAllowCleartextAuth = false, autologinSessions = false, retryOnAuthFailed = false;
    bool tunPersist = false, altProxy = false, dco = false, retry = false;
    std::string username = "", password = "", response = "", dynamicChallengeCookie = "";
    std::string proto = "", ipv6 = "", server = "", port = "", cipher_alg = "", gui_version = HUMMINGBIRD_FULL_NAME;
    std::string compress = "", privateKeyPassword = "", tlsVersionMinOverride = "", tlsCertProfileOverride = "";
    std::string proxyHost = "", proxyPort = "", proxyUsername = "", proxyPassword = "", peer_info = "", gremlin = "";
    std::string epki_cert_fn = "", epki_ca_fn = "", epki_key_fn = "";

#ifdef OPENVPN_REMOTE_OVERRIDE
    std::string remote_override_cmd;
#endif

    auto cleanup = Cleanup([]()
        {
            vpnClient = nullptr;
        });

    if(options.empty() == false)
    {
        optionError = optionParser->parseOptions(options);

        if(optionError != OptionParser::Error::OK)
        {
            std::cout << "ERROR: " + optionParser->getErrorDescription() << std::endl;

            return 1;
        }

        parserOptions = optionParser->getInvalidOptions();

        if(parserOptions.size() > 0)
        {
            std::cout << "Option " << parserOptions[0]->longName << " (" << parserOptions[0]->shortName << "): " << parserOptions[0]->error << std::endl;

            return 1;
        }
    }

    parserOptions = optionParser->getOptions();
    
    for(OptionParser::Option *option : parserOptions)
    {
        if(option->longName == "help")
        {
            usage();
            
            clean_up();
        }
        else if(option->longName == "ssl-debug")
        {
            try
            {
                sslDebugLevel = std::stoi(option->value);
            }
            catch(std::exception &e)
            {
                sslDebugLevel = 0;
            }
        }
        else if(option->longName == "epki-cert")
        {
            epki_cert_fn = option->value;
        }
        else if(option->longName == "epki-ca")
        {
            epki_ca_fn = option->value;
        }
        else if(option->longName == "epki-key")
        {
            epki_key_fn = option->value;
        }
#ifdef OPENVPN_REMOTE_OVERRIDE
        else if(option->longName == "remote-override")
        {
            remote_override_cmd = option->value;
        }

#endif

        else if(option->longName == "recover-network")
        {
            std::cout << "ERROR: --recover-network option must be used alone" << std::endl << std::endl;

            return 1;
        }
        else if(option->longName == "eval")
        {
            eval = true;
        }
        else if(option->longName == "cipher")
        {
            cipher_alg = option->value;
        }
        else if(option->longName == "tcp-queue-limit")
        {
            try
            {
                tcp_queue_limit = std::stoi(option->value);
            }
            catch(std::exception &e)
            {
                tcp_queue_limit = TCP_QUEUE_LIMIT_DEFAULT;
            }

            if(tcp_queue_limit < 0 || tcp_queue_limit > 65535)
            {
                std::cout << "ERROR: --tcp-queue-limit must be from 1 to 65535" << std::endl << std::endl;

                return 1;
            }
        }
        else if(option->longName == "ncp-disable")
        {
            ncp_disable = true;
        }
        else if(option->longName == "network-lock")
        {
            if(option->value != "on" && option->value != "iptables" && option->value != "nftables" && option->value != "pf" && option->value != "off")
            {
                std::cout << "ERROR: --network-lock option must be on, iptables, nftables, pf or off" << std::endl << std::endl;

                return 1;
            }

            if(option->value == "on")
                network_lock_mode = NetFilter::Mode::AUTO;
            else if(option->value == "iptables")
                network_lock_mode = NetFilter::Mode::IPTABLES;
            else if(option->value == "nftables")
                network_lock_mode = NetFilter::Mode::NFTABLES;
            else if(option->value == "pf")
                network_lock_mode = NetFilter::Mode::PF;
            else
                network_lock_mode = NetFilter::Mode::OFF;
        }
        else if(option->longName == "gui-version")
        {
            if(option->value.empty())
            {
                std::cout << "ERROR: invalid gui version" << std::endl << std::endl;

                return 1;
            }

            gui_version = option->value;
        }
        else if(option->longName == "ignore-dns-push")
        {
            ignore_dns_push = true;
        }
        else if(option->longName == "cache-password")
        {
            cachePassword = true;
        }
        else if(option->longName == "no-cert")
        {
            disableClientCert = true;
        }
        else if(option->longName == "username")
        {
            username = option->value;
        }
        else if(option->longName == "password")
        {
            password = option->value;
        }
        else if(option->longName == "response")
        {
            response = option->value;
        }
        else if(option->longName == "proto")
        {
            proto = option->value;
            
            if(proto != "udp" && proto != "tcp")
                OPENVPN_THROW_EXCEPTION("protocol must be \"udp\" or \"tcp\"");
        }
        else if(option->longName == "ipv6")
        {
            ipv6 = option->value;
        }
        else if(option->longName == "server")
        {
            server = option->value;
        }
        else if(option->longName == "port")
        {
            port = option->value;

            int iport;
            
            try
            {
                iport = std::stoi(port);
            }
            catch(std::exception &e)
            {
                OPENVPN_THROW_EXCEPTION("Illegal value for \"port\"");
            }

            if(iport < 0 || iport > 65535)
                OPENVPN_THROW_EXCEPTION("Port must be from 1 to 65535");
        }
        else if(option->longName == "timeout")
        {
            try
            {
                timeout = std::stoi(option->value);
            }
            catch(std::exception &e)
            {
                timeout = 0;
            }
        }
        else if(option->longName == "compress")
        {
            compress = option->value;
        }
        else if(option->longName == "pk-password")
        {
            privateKeyPassword = option->value;
        }
        else if(option->longName == "tvm-override")
        {
            tlsVersionMinOverride = option->value;
        }
        else if(option->longName == "tcprof-override")
        {
            tlsCertProfileOverride = option->value;
        }
        else if(option->longName == "proxy-host")
        {
            proxyHost = option->value;
        }
        else if(option->longName == "proxy-port")
        {
            proxyPort = option->value;
        }
        else if(option->longName == "proxy-username")
        {
            proxyUsername = option->value;
        }
        else if(option->longName == "proxy-password")
        {
            proxyPassword = option->value;
        }
        else if(option->longName == "proxy-basic")
        {
            proxyAllowCleartextAuth = true;
        }
        else if(option->longName == "alt-proxy")
        {
            altProxy = true;
        }

#ifdef ENABLE_DCO

        else if(option->longName == "dco")
        {
            dco = true;
        }

#endif

        else if(option->longName == "auto-sess")
        {
            autologinSessions = true;
        }
        else if(option->longName == "auth-retry")
        {
            retryOnAuthFailed = true;
        }
        else if(option->longName == "persist-tun")
        {
            tunPersist = true;
        }
        else if(option->longName == "def-keydir")
        {
            if(option->value == "bi" || option->value == "bidirectional")
                defaultKeyDirection = -1;
            else if(option->value == "0")
                defaultKeyDirection = 0;
            else if(option->value == "1")
                defaultKeyDirection = 1;
            else
                OPENVPN_THROW_EXCEPTION("bad default key-direction: " << option->value);
        }
        else if(option->longName == "dc")
        {
            dynamicChallengeCookie = option->value;
        }
        else if(option->longName == "peer-info")
        {
            peer_info = option->value;
        }
        else if(option->longName == "gremlin")
        {
            gremlin = option->value;
        }
    }

    do
    {
        retry = false;

        config.guiVersion = gui_version;

        config.content = read_profile(profile_filename.c_str(), nullptr);

        config.serverOverride = server;
        config.portOverride = port;
        config.protoOverride = proto;
        config.cipherOverrideAlgorithm = cipher_alg;
        config.connTimeout = timeout;
        config.compressionMode = compress;
        config.ipv6 = ipv6;
        config.tcpQueueLimit = tcp_queue_limit;
        config.disableNCP = ncp_disable;
        config.privateKeyPassword = privateKeyPassword;
        config.tlsVersionMinOverride = tlsVersionMinOverride;
        config.tlsCertProfileOverride = tlsCertProfileOverride;
        config.disableClientCert = disableClientCert;
        config.proxyHost = proxyHost;
        config.proxyPort = proxyPort;
        config.proxyUsername = proxyUsername;
        config.proxyPassword = proxyPassword;
        config.proxyAllowCleartextAuth = proxyAllowCleartextAuth;
        config.altProxy = altProxy;
        config.dco = dco;
        config.defaultKeyDirection = defaultKeyDirection;
        config.sslDebugLevel = sslDebugLevel;
        config.googleDnsFallback = false;
        config.autologinSessions = autologinSessions;
        config.retryOnAuthFailed = retryOnAuthFailed;
        config.tunPersist = tunPersist;
        config.gremlinConfig = gremlin;
        config.info = true;
        config.wintun = false;

        if(!epki_cert_fn.empty())
            config.externalPkiAlias = "epki"; // dummy string

        PeerInfo::Set::parse_csv(peer_info, config.peerInfo);

        // allow -s server override to reference a friendly name
        // in the config.
        //   setenv SERVER <HOST>/<FRIENDLY_NAME>

        if(!config.serverOverride.empty())
        {
            const ClientAPI::EvalConfig eval = ClientAPI::OpenVPNClient::eval_config_static(config);

            for(auto &se : eval.serverList)
            {
                if(config.serverOverride == se.friendlyName)
                {
                    config.serverOverride = se.server;

                    break;
                }
            }
        }

        if(eval)
        {
            const ClientAPI::EvalConfig eval = ClientAPI::OpenVPNClient::eval_config_static(config);

            std::cout << "EVAL PROFILE" << std::endl;
            std::cout << "error=" << eval.error << std::endl;
            std::cout << "message=" << eval.message << std::endl;
            std::cout << "userlockedUsername=" << eval.userlockedUsername << std::endl;
            std::cout << "profileName=" << eval.profileName << std::endl;
            std::cout << "cipher=" << eval.cipher << std::endl;
            std::cout << "friendlyName=" << eval.friendlyName << std::endl;
            std::cout << "autologin=" << eval.autologin << std::endl;
            std::cout << "externalPki=" << eval.externalPki << std::endl;
            std::cout << "staticChallenge=" << eval.staticChallenge << std::endl;
            std::cout << "staticChallengeEcho=" << eval.staticChallengeEcho << std::endl;
            std::cout << "privateKeyPasswordRequired=" << eval.privateKeyPasswordRequired << std::endl;
            std::cout << "allowPasswordSave=" << eval.allowPasswordSave << std::endl;

            if(!config.serverOverride.empty())
                std::cout << "server=" << config.serverOverride << std::endl;

            for(size_t i = 0; i < eval.serverList.size(); ++i)
            {
                const ClientAPI::ServerEntry& se = eval.serverList[i];

                std::cout << "Server[" << i << "] " << se.server << "/" << se.friendlyName << std::endl;
            }

            for(size_t i = 0; i < eval.remoteList.size(); ++i)
            {
                const ClientAPI::RemoteEntry& re = eval.remoteList[i];

                std::cout << "Remote[" << i << "] " << re.server << " Port: " << re.port << " Protocol: " << re.protocol << std::endl;
            }
        }
        else
        {
            try
            {
                client = new VpnClient(network_lock_mode, RESOURCE_DIRECTORY, SYSTEM_DNS_BACKUP_FILE, RESOLVDOTCONF_BACKUP);
            }
            catch(NetFilterException &e)
            {
                std::cout << "ERROR: " << e.what() << std::endl;

                if(client != nullptr)
                    delete client;

                clean_up();

                exit(1);
            }

            if(client == nullptr)
            {
                std::cout << "ERROR while creating OpenVPN client." << std::endl;

                clean_up();

                exit(1);
            }

            const ClientAPI::EvalConfig eval = client->eval_config(config);

            if(eval.error)
            {
                delete client;

                OPENVPN_THROW_EXCEPTION("eval config error: " << eval.message);
            }

            if(eval.autologin)
            {
                if(!username.empty() || !password.empty())
                    std::cout << "NOTE: creds were not needed" << std::endl;
            }
            else
            {
                if(username.empty())
                {
                    delete client;

                    OPENVPN_THROW_EXCEPTION("need creds");
                }

                ClientAPI::ProvideCreds creds;

                if(password.empty() && dynamicChallengeCookie.empty())
                    password = get_password("Password:");

                creds.username = username;
                creds.password = password;
                creds.response = response;
                creds.dynamicChallengeCookie = dynamicChallengeCookie;
                creds.replacePasswordWithSessionID = true;
                creds.cachePassword = cachePassword;

                ClientAPI::Status creds_status = client->provide_creds(creds);

                if(creds_status.error)
                {
                    delete client;

                    OPENVPN_THROW_EXCEPTION("creds error: " << creds_status.message);
                }
            }

            // external PKI

            if(!epki_cert_fn.empty())
            {
                client->epki_cert = read_text_utf8(epki_cert_fn);

                if(!epki_ca_fn.empty())
                    client->epki_ca = read_text_utf8(epki_ca_fn);

#if defined(USE_MBEDTLS)

                if(!epki_key_fn.empty())
                {
                    const std::string epki_key_txt = read_text_utf8(epki_key_fn);

                    client->epki_ctx.parse(epki_key_txt, "EPKI", privateKeyPassword);
                }
                else
                {
                    delete client;

                    OPENVPN_THROW_EXCEPTION("--epki-key must be specified");
                }

#endif
            }

#ifdef OPENVPN_REMOTE_OVERRIDE
            client->set_remote_override_cmd(remote_override_cmd);
#endif

            // start the client thread

            client->setConfig(config);
            client->setEvalConfig(eval);

            client->setNetworkLockMode(network_lock_mode);

            client->ignoreDnsPush(ignore_dns_push);

            start_connection_thread(client);

            // Get dynamic challenge response

            if(client->is_dynamic_challenge())
            {
                std::cout << "ENTER RESPONSE" << std::endl;

                std::getline(std::cin, response);

                if(!response.empty())
                {
                    dynamicChallengeCookie = client->dynamic_challenge_cookie();

                    retry = true;
                }
            }
            else
            {
                // print closing stats

                client->print_stats();
            }

            delete client;

            vpnClient = nullptr;
        }
    } while(retry);

    return ret;
}

void usage()
{
    std::cout << "usage: hummingbird [options] <config-file>" << std::endl;
    std::cout << "--help, -h            : show this help page" << std::endl;
    std::cout << "--version, -v         : show version info" << std::endl;
    std::cout << "--eval, -e            : evaluate profile only (standalone)" << std::endl;
    std::cout << "--username, -u        : username" << std::endl;
    std::cout << "--password, -p        : password" << std::endl;
    std::cout << "--response, -r        : static response" << std::endl;
    std::cout << "--dc, -D              : dynamic challenge/response cookie" << std::endl;
    std::cout << "--cipher, -C          : encrypt packets with specific cipher algorithm (alg)" << std::endl;
    std::cout << "--proto, -P           : protocol override (udp|tcp)" << std::endl;
    std::cout << "--server, -s          : server override" << std::endl;
    std::cout << "--port, -R            : port override" << std::endl;
    std::cout << "--tcp-queue-limit, -l : size of TCP packet queue (1-65535, default " << TCP_QUEUE_LIMIT_DEFAULT << ")" << std::endl;
    std::cout << "--ncp-disable, -n     : disable negotiable crypto parameters" << std::endl;
    std::cout << "--network-lock, -N    : network filter and lock mode (on|iptables|nftables|pf|off, default on)" << std::endl;
    std::cout << "--gui-version, -E     : set custom gui version (text)" << std::endl;
    std::cout << "--ignore-dns-push, -i : ignore DNS push request and use system DNS settings" << std::endl;

#ifdef OPENVPN_REMOTE_OVERRIDE
    std::cout << "--remote-override     : command to run to generate next remote (returning host,ip,port,proto)" << std::endl;
#endif

    std::cout << "--ipv6, -6            : combined IPv4/IPv6 tunnel (yes|no|default)" << std::endl;
    std::cout << "--timeout, -t         : timeout" << std::endl;
    std::cout << "--compress, -c        : compression mode (yes|no|asym)" << std::endl;
    std::cout << "--pk-password, -z     : private key password" << std::endl;
    std::cout << "--tvm-override, -M    : tls-version-min override (disabled, default, tls_1_x)" << std::endl;
    std::cout << "--tcprof-override, -X : tls-cert-profile override (" <<

#ifdef OPENVPN_USE_TLS_MD5
        "insecure, " <<
#endif
        "legacy, preferred, etc.)" << std::endl;

    std::cout << "--proxy-host, -y      : HTTP proxy hostname/IP" << std::endl;
    std::cout << "--proxy-port, -q      : HTTP proxy port" << std::endl;
    std::cout << "--proxy-username, -U  : HTTP proxy username" << std::endl;
    std::cout << "--proxy-password, -W  : HTTP proxy password" << std::endl;
    std::cout << "--proxy-basic, -b     : allow HTTP basic auth" << std::endl;
    std::cout << "--alt-proxy, -A       : enable alternative proxy module" << std::endl;

#ifdef ENABLE_DCO

    std::cout << "--dco, -d             : enable data channel offload" << std::endl;

#endif

    std::cout << "--cache-password, -H  : cache password" << std::endl;
    std::cout << "--no-cert, -x         : disable client certificate" << std::endl;
    std::cout << "--def-keydir, -k      : default key direction ('bi', '0', or '1')" << std::endl;
    std::cout << "--ssl-debug           : SSL debug level" << std::endl;
    std::cout << "--auto-sess, -a       : request autologin session" << std::endl;
    std::cout << "--auth-retry, -Y      : retry connection on auth failure" << std::endl;
    std::cout << "--persist-tun, -j     : keep TUN interface open across reconnects" << std::endl;
    std::cout << "--peer-info, -I       : peer info key/value list in the form K1=V1,K2=V2,..." << std::endl;
    std::cout << "--gremlin, -G         : gremlin info (send_delay_ms, recv_delay_ms, send_drop_prob, recv_drop_prob)" << std::endl;
    std::cout << "--epki-ca             : simulate external PKI cert supporting intermediate/root certs" << std::endl;
    std::cout << "--epki-cert           : simulate external PKI cert" << std::endl;
    std::cout << "--epki-key            : simulate external PKI private key" << std::endl;
    std::cout << "--recover-network     : recover network settings after a crash or unexpected exit" << std::endl << std::endl;

    aboutDevelopmentCredits();
}

void aboutDevelopmentCredits()
{
    std::cout << "Open Source Project by AirVPN (https://airvpn.org)" << std::endl << std::endl;
    std::cout << "Linux and macOS design, development and coding by ProMIND" << std::endl << std::endl;

    std::cout << "Special thanks to the AirVPN community for the valuable help," << std::endl;
    std::cout << "support, suggestions and testing." << std::endl << std::endl;
}

bool setup_options()
{
    bool result;

    if(optionParser == nullptr)
        return false;

    result = optionParser->addConfigOption("u", "username", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("p", "password", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("r", "response", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("D", "dc", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("P", "proto", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("6", "ipv6", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("s", "server", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("R", "port", OptionParser::Type::INTEGER);
    result &= optionParser->addConfigOption("C", "cipher", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("l", "tcp-queue-limit", OptionParser::Type::INTEGER);
    result &= optionParser->addConfigOption("n", "ncp-disable", OptionParser::Type::OPTION);
    result &= optionParser->addConfigOption("i", "ignore-dns-push", OptionParser::Type::OPTION);
    result &= optionParser->addConfigOption("N", "network-lock", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("E", "gui-version", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("t", "timeout", OptionParser::Type::INTEGER);
    result &= optionParser->addConfigOption("c", "compress", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("z", "pk-password", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("M", "tvm-override", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("y", "proxy-host", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("q", "proxy-port", OptionParser::Type::INTEGER);
    result &= optionParser->addConfigOption("U", "proxy-username", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("W", "proxy-password", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("I", "peer-info", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("G", "gremlin", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("b", "proxy-basic", OptionParser::Type::OPTION);
    result &= optionParser->addConfigOption("A", "alt-proxy", OptionParser::Type::OPTION);

#ifdef ENABLE_DCO

    result &= optionParser->addConfigOption("d", "dco", OptionParser::Type::OPTION);

#endif

    result &= optionParser->addConfigOption("e", "eval", OptionParser::Type::OPTION);
    result &= optionParser->addConfigOption("H", "cache-password", OptionParser::Type::OPTION);
    result &= optionParser->addConfigOption("x", "no-cert", OptionParser::Type::OPTION);
    result &= optionParser->addConfigOption("j", "persist-tun", OptionParser::Type::OPTION);
    result &= optionParser->addConfigOption("k", "def-keydir", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("v", "version", OptionParser::Type::OPTION);
    result &= optionParser->addConfigOption("h", "help", OptionParser::Type::OPTION);
    result &= optionParser->addConfigOption("a", "auto-sess", OptionParser::Type::OPTION);
    result &= optionParser->addConfigOption("Y", "auth-retry", OptionParser::Type::OPTION);
    result &= optionParser->addConfigOption("X", "tcprof-override", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("", "ssl-debug", OptionParser::Type::INTEGER);
    result &= optionParser->addConfigOption("", "epki-cert", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("", "epki-ca", OptionParser::Type::STRING);
    result &= optionParser->addConfigOption("", "epki-key", OptionParser::Type::STRING);


#ifdef OPENVPN_REMOTE_OVERRIDE

    result &= optionParser->addConfigOption("", "remote-override", OptionParser::Type::STRING);

#endif

    result &= optionParser->addConfigOption("", "recover-network", OptionParser::Type::OPTION);

    return result;
}

void global_log(std::string msg)
{
    std::string ts = date_time();
  
    std::cout << ts << ' ' << msg << std::endl << std::flush;
}

int main(int argc, char *argv[])
{
    int retval = 0;
    VpnClient *client = nullptr;
    std::vector<std::string> options;
    std::string profile_name;

#ifdef OPENVPN_LOG_LOGBASE_H

    LogBaseSimple log;

#endif

    show_intro();

    if(check_if_root() == false)
    {
        std::cout << "You need to be root in order to run this program." << std::endl << std::endl;

        return 1;
    }

    if(argc < 2)
    {
        usage();
        
        return 1;
    }

    if(argc == 2)
    {
        if(strcmp(argv[1], "--recover-network") == 0)
        {
            if(access(HUMMINGBIRD_LOCK_FILE, F_OK) != 0)
            {
                std::cout << "It seems this program has properly exited in its last run and it has already" << std::endl;
                std::cout << "restored network settings on exit." << std::endl;
                std::cout << "Network recovery is not needed." << std::endl << std::endl;
            }
            else
            {
                try
                {
                    client = new VpnClient(NetFilter::Mode::OFF, RESOURCE_DIRECTORY, SYSTEM_DNS_BACKUP_FILE, RESOLVDOTCONF_BACKUP);
                
                    if(client != nullptr)
                    {
                        client->restoreNetworkSettings(VpnClient::RestoreNetworkMode::FULL, true);

                        delete client;
                    }
                    else
                        std::cout << "ERROR: Cannot create VpnClient object." << std::endl;
                }
                catch(NetFilterException &e)
                {
                    std::cout << "ERROR: " << e.what() << std::endl;

                    if(client)
                        delete client;
                }

                std::cout << std::endl;
            }

            clean_up();

            return 0;
        }
        else if(strcmp(argv[1], "--help") == 0)
        {
            usage();
            
            clean_up();
            
            return 0;
        }
        else if(strcmp(argv[1], "--version") == 0)
        {
            std::cout << ClientAPI::OpenVPNClient::platform() << std::endl;
            std::cout << ClientAPI::OpenVPNClient::copyright() << std::endl << std::endl;
            std::cout << "Released under the GNU General Public License version 3 (GPLv3)" << std::endl << std::endl;

            aboutDevelopmentCredits();
            
            clean_up();
            
            return 0;
        }
    }

    if(init_check() == false)
        return 1;

    optionParser = new OptionParser();

    if(optionParser == nullptr)
    {
        std::cout << "Cannot create OptionParser object. Exiting." << std::endl;

        clean_up();
    }

    if(setup_options() == false)
    {
        std::cout << "Cannot setup options. Exiting." << std::endl;

        clean_up();
    }

    std::cout << date_time() << " System and service manager in use is " << AirVPNTools::getInitSystemName(AirVPNTools::getInitSystemType()) << std::endl;

    try
    {
        profile_name = argv[argc - 1];

        if(profile_name[0] == '-') // Last argument is an option. No openvpn profile provided
        {
            usage();

            clean_up();
            
            return 1;
        }

        if(access(profile_name.c_str(), F_OK) != 0)
        {
            std::cout << "ERROR: profile " << profile_name << " not found" << std::endl;

            clean_up();
            
            return 1;
        }

        if(argc > 2)
        {
            for(int i = 1; i < argc - 1; i++)
                options.push_back(argv[i]);
        }
        else
            options.clear();

        retval = openvpn_client(options, profile_name);
    }
    catch(const std::exception& e)
    {
        std::cout << "ERROR: " << e.what() << std::endl;

        retval = 1;
    }

    if(!clean_up())
        retval = 1;

    return retval;
}


