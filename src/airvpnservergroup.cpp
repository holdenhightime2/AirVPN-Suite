/*
 * airvpnservergroup.cpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2021 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "include/airvpnservergroup.hpp"

AirVPNServerGroup::AirVPNServerGroup()
{
}

AirVPNServerGroup::~AirVPNServerGroup()
{
}

bool AirVPNServerGroup::add(ServerGroup s)
{
    bool found = false;

    for(std::map<int, ServerGroup>::iterator it = serverGroupDatabase.begin(); it != serverGroupDatabase.end() and found == false; it++)
    {
        if(it->second.group == s.group)
            found = true;
    }

    if(found == false)
        serverGroupDatabase.insert(std::make_pair(s.group, s));

    return !found;
}

bool AirVPNServerGroup::getGroupIPv4Support(int group)
{
    bool result = false;

    std::map<int, ServerGroup>::iterator it = serverGroupDatabase.find(group);

    if(it != serverGroupDatabase.end())
        result = it->second.ipv4Support;

    return result;
}

bool AirVPNServerGroup::getGroupIPv6Support(int group)
{
    bool result = false;

    std::map<int, ServerGroup>::iterator it = serverGroupDatabase.find(group);

    if(it != serverGroupDatabase.end())
        result = it->second.ipv6Support;

    return result;
}

bool AirVPNServerGroup::getGroupSupportCheck(int group)
{
    bool result = false;

    std::map<int, ServerGroup>::iterator it = serverGroupDatabase.find(group);

    if(it != serverGroupDatabase.end())
        result = it->second.checkSupport;

    return result;
}

std::vector<int> AirVPNServerGroup::getGroupTlsCiphers(int group)
{
    std::vector<int> result;

    result.clear();

    std::map<int, ServerGroup>::iterator it = serverGroupDatabase.find(group);

    if(it != serverGroupDatabase.end())
        result = it->second.tlsCiphers;

    return result;
}

std::vector<int> AirVPNServerGroup::getGroupTlsSuiteCiphers(int group)
{
    std::vector<int> result;

    result.clear();

    std::map<int, ServerGroup>::iterator it = serverGroupDatabase.find(group);

    if(it != serverGroupDatabase.end())
        result = it->second.tlsSuiteCiphers;

    return result;
}

std::vector<int> AirVPNServerGroup::getGroupDataCiphers(int group)
{
    std::vector<int> result;

    result.clear();

    std::map<int, ServerGroup>::iterator it = serverGroupDatabase.find(group);

    if(it != serverGroupDatabase.end())
        result = it->second.dataCiphers;

    return result;
}

void AirVPNServerGroup::reset()
{
    serverGroupDatabase.clear();
}
