/*
 * airvpnserverprovider.cpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2021 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "include/airvpnserverprovider.hpp"
#include "include/airvpntools.hpp"

#include <fstream>

extern void global_log(std::string s);
extern void global_syslog(std::string s);

AirVPNServerProvider::AirVPNServerProvider(AirVPNUser *user, std::string resourceDirectory)
{
    airVPNManifest = new AirVPNManifest(resourceDirectory);

    loadConnectionPriorities(resourceDirectory);

    airVPNUser = user;

    userIP = "";
    userCountry = "";
    tlsMode = TLSMode::NOT_SET;
    supportIPv4 = true;
    supportIPv6 = true;

    serverWhitelist.clear();
    serverBlacklist.clear();
    countryWhitelist.clear();
    countryBlacklist.clear();

    countryContinent = new CountryContinent(resourceDirectory);
}

AirVPNServerProvider::~AirVPNServerProvider()
{
    if(airVPNManifest != nullptr)
        delete airVPNManifest;

    if(countryContinent != nullptr)
        delete countryContinent;
}

bool AirVPNServerProvider::compareServerScore(AirVPNServer s1, AirVPNServer s2)
{
    return (s1.getScore() < s2.getScore());
}

void AirVPNServerProvider::loadConnectionPriorities(std::string resourceDirectory)
{
    std::string line, fileName;
    std::vector<std::string> item;

    fileName = resourceDirectory;
    fileName += "/";
    fileName += AIRVPN_CONNECTION_PRIORITY_FILE_NAME;

    std::ifstream connectionPriorityFile(fileName);

    if(connectionPriorityFile.is_open())
    {
        while(std::getline(connectionPriorityFile, line))
        {
            item = AirVPNTools::split(line, "->");

            if(item.size() == 2)
                connectionPriority.insert(std::make_pair(AirVPNTools::trim(item[0]), AirVPNTools::trim(item[1])));
        }

        connectionPriorityFile.close();
    }
    else
        global_syslog("ERROR: AirVPNServerProvider::loadConnectionPriorities() - cannot open " + fileName);
}

std::string AirVPNServerProvider::getUserIP()
{
    return userIP;
}

void AirVPNServerProvider::setUserIP(std::string u)
{
    userIP = u;
}

std::string AirVPNServerProvider::getUserCountry()
{
    return userCountry;
}

void AirVPNServerProvider::setUserCountry(std::string c)
{
    userCountry = c;

    userContinent = countryContinent->getCountryContinent(userCountry);
}

AirVPNServerProvider::TLSMode AirVPNServerProvider::getTlsMode()
{
    return tlsMode;
}

void AirVPNServerProvider::setTlsMode(AirVPNServerProvider::TLSMode m)
{
    tlsMode = m;
}

bool AirVPNServerProvider::getSupportIPv4()
{
    return supportIPv4;
}

void AirVPNServerProvider::setSupportIPv4(bool s)
{
    supportIPv4 = s;
}

bool AirVPNServerProvider::getSupportIPv6()
{
    return supportIPv6;
}

void AirVPNServerProvider::setSupportIPv6(bool s)
{
    supportIPv6 = s;
}

std::vector<std::string> AirVPNServerProvider::getServerWhitelist()
{
    return serverWhitelist;
}

void AirVPNServerProvider::setServerWhitelist(std::vector<std::string> list)
{
    serverWhitelist = list;
}

std::vector<std::string> AirVPNServerProvider::getServerBlacklist()
{
    return serverBlacklist;
}

void AirVPNServerProvider::setServerBlacklist(std::vector<std::string> list)
{
    serverBlacklist = list;
}

std::vector<std::string> AirVPNServerProvider::getCountryWhitelist()
{
    return countryWhitelist;
}

void AirVPNServerProvider::setCountryWhitelist(std::vector<std::string> list)
{
    countryWhitelist = list;
}

std::vector<std::string> AirVPNServerProvider::getCountryBlacklist()
{
    return countryBlacklist;
}

void AirVPNServerProvider::setCountryBlacklist(std::vector<std::string> list)
{
    countryBlacklist = list;
}

std::vector<AirVPNServer> AirVPNServerProvider::getFilteredServerList()
{
    bool include = false, ipEntryFound = false;
    bool forbidLocalServerConnection = true; // da config .rc
    std::vector<AirVPNServer> serverList, priorityServerList, filteredServerList;
    std::vector<std::string> userConnectionPriority;
    std::vector<int> cipherList;
    std::map<int, std::string> ipEntry;
    int validItems = 0, validItemsForInclusion = 1;

    if(airVPNManifest == nullptr)
    {
        global_log("AirVPNServerProdiver::getFilteredServerList(): airVPNManifest is null");

        return filteredServerList;
    }

    cipherList = CipherDatabase::getMatchingCiphersArrayList("CIPHER", CipherDatabase::CipherType::DATA);

    if(serverWhitelist.size() == 0 && countryWhitelist.size() == 0)
        serverList = airVPNManifest->getAirVpnServerList();
    else
    {
        serverList.clear();

        if(serverWhitelist.size() > 0)
        {
            for(std::string name : serverWhitelist)
            {
                AirVPNServer airVPNServer = airVPNManifest->getServerByName(name);

                if(airVPNServer.getName() != "" && airVPNServer.isAvailable(&cipherList))
                    serverList.push_back(airVPNServer);
            }
        }

        if(countryWhitelist.size() > 0)
        {
            for(AirVPNServer server : airVPNManifest->getAirVpnServerList())
            {
                if(std::find(countryWhitelist.begin(), countryWhitelist.end(), AirVPNTools::toUpper(server.getCountryCode())) != countryWhitelist.end() &&
                    server.isAvailable(&cipherList) &&
                    std::find(serverWhitelist.begin(), serverWhitelist.end(), AirVPNTools::toLower(server.getName())) == serverWhitelist.end())
                    serverList.push_back(server);
            }
        }
    }

    userConnectionPriority = getUserConnectionPriority();

    if(forbidLocalServerConnection == false)
        userConnectionPriority.insert(userConnectionPriority.begin(), userCountry);

    if(userConnectionPriority.size() > 0 && serverList.size() > 0)
    {
        priorityServerList.clear();

        for(int srv = 0; srv < serverList.size(); srv++)
        {
            std::string area = "";
            int penalty = 0;

            include = false;

            AirVPNServer airVPNServer = serverList[srv];

            for(int i = 0; i < userConnectionPriority.size() && include == false; i++)
            {
                area = userConnectionPriority[i];

                if(area.length() == 2)
                {
                    if(area == airVPNServer.getCountryCode())
                    {
                        include = true;

                        penalty = i;
                    }
                }
                else if(area.length() == 3)
                {
                    if(area == airVPNServer.getContinent())
                    {
                        include = true;

                        penalty = i;
                    }
                }
                else
                {
                    for(std::string place : AirVPNTools::split(airVPNServer.getLocation(), ","))
                    {
                        if(area == place)
                        {
                            include = true;

                            penalty = i;
                        }
                    }
                }
            }

            if(userCountry == airVPNServer.getCountryCode()  && forbidLocalServerConnection)
                include = false;

            if(!airVPNServer.isAvailable(&cipherList))
                include = false;

            if(std::find(serverBlacklist.begin(), serverBlacklist.end(), AirVPNTools::toLower(airVPNServer.getName())) != serverBlacklist.end())
                include = false;

            if(std::find(countryBlacklist.begin(), countryBlacklist.end(), AirVPNTools::toUpper(airVPNServer.getCountryCode())) != countryBlacklist.end())
                include = false;

            if(include)
            {
                airVPNServer.setScore(airVPNServer.getScore() + (10000 * penalty));

                priorityServerList.push_back(airVPNServer);
            }
        }
    }
    else
        priorityServerList = serverList;

    if(priorityServerList.size() == 0)
        return priorityServerList;

    filteredServerList.clear();

    if(supportIPv4)
        validItemsForInclusion++;

    if(supportIPv6)
        validItemsForInclusion++;

    for(AirVPNServer server : priorityServerList)
    {
        validItems = 0;

        if(supportIPv4 && server.getSupportIPv4() == true)
            validItems++;

        if(supportIPv6 && server.getSupportIPv6() == true)
            validItems++;

        ipEntry = server.getEntryIPv4();
        ipEntryFound = false;

        switch(tlsMode)
        {
            case TLS_AUTH:
            {
                if(!ipEntry[1].empty())
                    ipEntryFound = true;

                if(!ipEntry[2].empty())
                    ipEntryFound = true;

                if(ipEntryFound)
                    validItems++;
            }
            break;

            case TLS_CRYPT:
            {
                if(!ipEntry[3].empty())
                    ipEntryFound = true;

                if(!ipEntry[4].empty())
                    ipEntryFound = true;

                if (ipEntryFound)
                    validItems++;
            }
            break;
            
            default:    break;
        }

        if(validItems == validItemsForInclusion)
            filteredServerList.push_back(server);
    }

    std::sort(filteredServerList.begin(), filteredServerList.end(), compareServerScore);

    return filteredServerList;
}

std::vector<std::string> AirVPNServerProvider::getUserConnectionPriority()
{
    std::vector<std::string> areaList;
    std::string areaPriority;
    std::map<std::string, std::string>::iterator it;
    int i;

    areaList.clear();

    if(userCountry != "RU")
    {
        it = connectionPriority.find(userCountry);

        if(it != connectionPriority.end())
            areaPriority = it->second;
        else
            areaPriority = "";
    }
    else
    {
        std::string ruArea = "";

        if(airVPNUser->getUserLongitude() < 103.851959)
            ruArea = "R1";
        else
            ruArea = "R2";

        it = connectionPriority.find(ruArea);

        if(it != connectionPriority.end())
            areaPriority = it->second;
        else
            areaPriority = "";
    }

    if(!areaPriority.empty())
    {
        std::vector<std::string> priority = AirVPNTools::split(areaPriority, ",");

        if(priority.size() > 0)
        {
            for(i = 0; i < priority.size(); i++)
                areaList.push_back(priority[i]);
        }
    }

    it = connectionPriority.find(userContinent);
    
    if(it != connectionPriority.end())
        areaPriority = it->second;
    else
        areaPriority = "";

    if(!areaPriority.empty())
    {
        std::vector<std::string> priority = AirVPNTools::split(areaPriority, ",");

        if(priority.size() > 0)
        {
            for(i = 0; i < priority.size(); i++)
                areaList.push_back(priority[i]);
        }
    }

    if(areaList.empty())
    {
        it = connectionPriority.find("DEFAULT");
        
        if(it != connectionPriority.end())
            areaPriority = it->second;
        else
            areaPriority = "";

        if(!areaPriority.empty())
        {
            std::vector<std::string> priority = AirVPNTools::split(areaPriority, ",");

            if(priority.size() > 0)
            {
                for(i = 0; i < priority.size(); i++)
                    areaList.push_back(priority[i]);
            }
        }
    }

    return areaList;
}
