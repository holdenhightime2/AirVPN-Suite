/*
 * airvpnserver.cpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2021 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "include/airvpnserver.hpp"
#include "include/airvpnmanifest.hpp"
#include "include/airvpntools.hpp"
#include "include/cipherdatabase.hpp"

#include <algorithm>

AirVPNServer::AirVPNServer(std::string resourceDirectory)
{
    name = "";
    continent = "";
    countryCode = "";
    region = "";
    location = "";
    bandWidth = 0;
    maxBandWidth = 0;
    users = 0;
    supportIPv4 = false;
    supportIPv6 = false;
    entryIPv4.clear();
    entryIPv6.clear();
    exitIPv4 = "";
    exitIPv6 = "";
    warningOpen = "";
    warningClosed = "";
    scoreBase = 0;
    score = WORST_SCORE;
    supportCheck = false;
    group = -1;
    tlsCiphers.clear();
    tlsSuiteCiphers.clear();
    dataCiphers.clear();

    countryContinent = new CountryContinent(resourceDirectory);
}

AirVPNServer::~AirVPNServer()
{
}

std::string AirVPNServer::getName()
{
    return name;
}

void AirVPNServer::setName(std::string n)
{
    name = n;
}

std::string AirVPNServer::getContinent()
{
    return continent;
}

void AirVPNServer::setContinent(std::string c)
{
    continent = c;
}

std::string AirVPNServer::getCountryCode()
{
    return countryCode;
}

std::string AirVPNServer::getCountryName()
{
    return countryContinent->getCountryName(countryCode);
}

void AirVPNServer::setCountryCode(std::string c)
{
    countryCode = c;

    continent = countryContinent->getCountryContinent(c);
}

std::string AirVPNServer::getRegion()
{
    return region;
}

void AirVPNServer::setRegion(std::string r)
{
    region = r;
}

std::string AirVPNServer::getLocation()
{
    return location;
}

void AirVPNServer::setLocation(std::string l)
{
    location = l;
}

long long AirVPNServer::getBandWidth()
{
    return bandWidth;
}

void AirVPNServer::setBandWidth(long long b)
{
    bandWidth = b;
}

long long AirVPNServer::getMaxBandWidth()
{
    return maxBandWidth;
}

void AirVPNServer::setMaxBandWidth(long long b)
{
    maxBandWidth = b;
}

long long AirVPNServer::getEffectiveBandWidth()
{
    return (2 * (bandWidth * 8));
}

int AirVPNServer::getUsers()
{
    return users;
}

void AirVPNServer::setUsers(int u)
{
    users = u;
}

int AirVPNServer::getMaxUsers()
{
    return maxUsers;
}

void AirVPNServer::setMaxUsers(int u)
{
    maxUsers = u;
}

double AirVPNServer::getUserLoad()
{
    return ((double)users * 100.0) / (double)maxUsers;
}

bool AirVPNServer::getSupportIPv4()
{
    return supportIPv4;
}

void AirVPNServer::setSupportIPv4(bool s)
{
    supportIPv4 = s;
}

bool AirVPNServer::getSupportIPv6()
{
    return supportIPv6;
}

void AirVPNServer::setSupportIPv6(bool s)
{
    supportIPv6 = s;
}

std::map<int, std::string> AirVPNServer::getEntryIPv4()
{
    return entryIPv4;
}

void AirVPNServer::setEntryIPv4(std::map<int, std::string> l)
{
    entryIPv4 = l;
}

void AirVPNServer::setEntryIPv4(int entry, std::string IPv4)
{
    entryIPv4.insert(std::make_pair(entry, IPv4));
}

std::map<int, std::string> AirVPNServer::getEntryIPv6()
{
    return entryIPv6;
}

void AirVPNServer::setEntryIPv6(std::map<int, std::string> l)
{
    entryIPv6 = l;
}

void AirVPNServer::setEntryIPv6(int entry, std::string IPv6)
{
    entryIPv6.insert(std::make_pair(entry, IPv6));
}

std::string AirVPNServer::getExitIPv4()
{
    return exitIPv4;
}

void AirVPNServer::setExitIPv4(std::string e)
{
    exitIPv4 = e;
}

std::string AirVPNServer::getExitIPv6()
{
    return exitIPv6;
}

void AirVPNServer::setExitIPv6(std::string e)
{
    exitIPv6 = e;
}

std::string AirVPNServer::getWarningOpen()
{
    return warningOpen;
}

void AirVPNServer::setWarningOpen(std::string w)
{
    warningOpen = w;
}

std::string AirVPNServer::getWarningClosed()
{
    return warningClosed;
}

void AirVPNServer::setWarningClosed(std::string w)
{
    warningClosed = w;
}

int AirVPNServer::getScoreBase()
{
    return scoreBase;
}

void AirVPNServer::setScoreBase(int s)
{
    scoreBase = s;
}

bool AirVPNServer::getSupportCheck()
{
    return supportCheck;
}

void AirVPNServer::setSupportCheck(bool s)
{
    supportCheck = s;
}

int AirVPNServer::getLoad()
{
    return AirVPNTools::getLoad(bandWidth, maxBandWidth);
}

std::vector<int> AirVPNServer::getTlsCiphers()
{
    return tlsCiphers;
}

std::string AirVPNServer::getTlsCipherNames()
{
    return CipherDatabase::getCipherNameList(CipherDatabase::CipherType::TLS, tlsCiphers);
}

void AirVPNServer::setTlsCiphers(std::vector<int> list)
{
    tlsCiphers = list;
}

bool AirVPNServer::hasTlsCipher(int c)
{
    if(tlsCiphers.empty())
        return false;

    return (std::find(tlsCiphers.begin(), tlsCiphers.end(), c) != tlsCiphers.end());
}

bool AirVPNServer::hasTlsCipher(std::vector<int> *cipherList)
{
    bool serverHasCipher = false;

    for(int c : *cipherList)
    {
        if(hasTlsCipher(c))
            serverHasCipher = true;
    }

    return serverHasCipher;
}

std::vector<int> AirVPNServer::getTlsSuiteCiphers()
{
    return tlsSuiteCiphers;
}

std::string AirVPNServer::getTlsSuiteCipherNames()
{
    return CipherDatabase::getCipherNameList(CipherDatabase::CipherType::TLS_SUITE, tlsSuiteCiphers);
}

void AirVPNServer::setTlsSuiteCiphers(std::vector<int> list)
{
    tlsSuiteCiphers = list;
}

bool AirVPNServer::hasTlsSuiteCipher(int c)
{
    if(tlsSuiteCiphers.empty())
        return false;

    return (std::find(tlsSuiteCiphers.begin(), tlsSuiteCiphers.end(), c) != tlsSuiteCiphers.end());
}

bool AirVPNServer::hasTlsSuiteCipher(std::vector<int> *cipherList)
{
    bool serverHasCipher = false;

    for(int c : *cipherList)
    {
        if(hasTlsSuiteCipher(c))
            serverHasCipher = true;
    }

    return serverHasCipher;
}

std::vector<int> AirVPNServer::getDataCiphers()
{
    return dataCiphers;
}

std::string AirVPNServer::getDataCipherNames()
{
    return CipherDatabase::getCipherNameList(CipherDatabase::CipherType::DATA, dataCiphers);
}

void AirVPNServer::setDataCiphers(std::vector<int> list)
{
    dataCiphers = list;
}

bool AirVPNServer::hasDataCipher(int c)
{
    if(dataCiphers.empty())
        return false;

    return (std::find(dataCiphers.begin(), dataCiphers.end(), c) != dataCiphers.end());
}

bool AirVPNServer::hasDataCipher(std::vector<int> *cipherList)
{
    if(cipherList == nullptr || cipherList->empty())
        return true;

    bool serverHasCipher = false;

    for(int c : *cipherList)
    {
        if(hasDataCipher(c))
            serverHasCipher = true;
    }

    return serverHasCipher;
}

void AirVPNServer::computeServerScore()
{
    double s = 0;

    if(maxBandWidth == 0 || !warningOpen.empty() || !warningClosed.empty())
    {
        score = WORST_SCORE;

        return;
    }

    s = (double)scoreBase / AirVPNManifest::getSpeedFactor();
    s += (double)getLoad() * AirVPNManifest::getLoadFactor();
    s += (double)users * getUserLoad();

    score = (int)s;
}

int AirVPNServer::getScore()
{
    return score;
}

void AirVPNServer::setScore(int s)
{
    score = s;
}

bool AirVPNServer::isAvailable()
{
    return warningClosed.empty();
}

bool AirVPNServer::isAvailable(std::vector<int> *cipherList)
{
    return warningClosed.empty() & hasDataCipher(cipherList);
}
