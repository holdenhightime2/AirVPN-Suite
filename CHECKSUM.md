# AirVPN Suite for Linux

#### AirVPN's free and open source OpenVPN 3 suite based on AirVPN's OpenVPN 3 library fork

### Version 1.1.0 - Release date 4 June 2021


## Note on Checksum Files

AirVPN Suite is an open source project and, as such, its source code can be
downloaded, forked and modified by anyone who wants to create a derivative
project or build it on his or her computer. This also means the source code can
be tampered or modified in a malicious way, therefore creating a binary version
of the suite which may act harmfully, destroy or steal your data, redirecting
your network traffic and data while pretending to be the "real" AirVPN Suite
client genuinely developed and supported by AirVPN.

For this reason, we cannot guarantee forked, modified and custom compiled
versions of AirVPN Suite to be compliant to our specifications, development and
coding guidelines and style, including our security standards. These projects,
of course, may also be better and more efficient than our release, however we
cannot guarantee or provide help for the job of others.

You are therefore strongly advised to check and verify the checksum codes found
in the `.sha512` files to exactly correspond to the ones below, that is, the
checksum we have computed from the sources and distribution files directly
compiled and built by AirVPN. This will make you sure about the origin and
authenticity of the suite. Please note the files contained in the distribution
tarballs are created from the very source code available in the master branch of
the [official AirVPN Suite's repository](https://gitlab.com/AirVPN/AirVPN-Suite).


### Checksum codes for Version 1.1.0

The checksum codes contained in files `AirVPN-Suite-<arch>-1.1.0.<archive>.sha512`
must correspond to the codes below in order to prove they are genuinely created
and distributed by AirVPN.

## Linux i686

`AirVPN-Suite-i686-1.1.0.tar.gz`:
ee24bffe3ff32e35cfde91caf8b529353f236ba141a3bf3bf4f6a2da4a7ecfde119bdf35e32a5be6800e7a72c9e075d9b98dfbe6a6b86557c833b1c196141836


## Linux x86_64

`AirVPN-Suite-x86_64-1.1.0.tar.gz`:
0af2c343b76e215f5e65e895b0b0264f330daf38eb22767374a3f0b05b008a8d8514339a92fa9ebd34e870b311969948527333424d40ec5a56838c22499b7f47


## Linux ARM32

`AirVPN-Suite-armv7l-1.1.0.tar.gz`:
a621c8e6b56385ce905816318cf93970d3af43b172c69a19b8f9d159526f1d458e0275a7eab0a37da1e9d2f9ad9a5b5d72e7b11b1278eddb6bd6c79b5de26bd1


## Linux ARM64

`AirVPN-Suite-aarch64-1.1.0.tar.gz`:
7daae2f4f7dc36f30ab8dccc2a0b2c17349bf4f73f86a3461e3f2726436a41b4c33d9c044e5a866a4db514089c35809d09dde33af2a27f586b85fe86cf4a5b2e
