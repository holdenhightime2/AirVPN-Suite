#!/bin/sh

#
# Build static binary for bluetit and distribution tarball
#
# Version 1.0 - ProMIND
#

BASE_NAME=bluetit
SSL_LIB_TYPE=OPENSSL

OS_NAME=$(uname)
OS_ARCH_NAME=$(uname -m)

MIN_MACOS_VERSION=10.13

INC_DIR=..
OPENVPN3=${INC_DIR}/openvpn3-airvpn
ASIO=${INC_DIR}/asio

VERSION=$(grep -r "#define BLUETIT_VERSION" src/include/btcommon.h | cut -f 2 -d \" | sed -e 's/ /-/g')

case $OS_NAME in
    Linux)
        OS_ARCH_NAME=$(uname -m)
        BIN_FILE=${BASE_NAME}-linux-${OS_ARCH_NAME}
        SHA_CMD=sha512sum
        DBUS_INCLUDE=$(pkg-config --cflags --libs dbus-1)
        DBUS_LIBS=$(pkg-config --cflags --libs dbus-1)

        echo "Building static ${BASE_NAME} ${VERSION} for Linux ${OS_ARCH_NAME}"

        SOURCES="src/bluetit.cpp \
                 src/dbusconnector.cpp \
                 src/localnetwork.cpp \
                 src/dnsmanager.cpp \
                 src/netfilter.cpp \
                 src/optionparser.cpp \
                 src/rcparser.cpp \
                 src/base64.cpp \
                 src/airvpntools.cpp \
                 src/airvpnmanifest.cpp \
                 src/airvpnserver.cpp \
                 src/airvpnuser.cpp \
                 src/countrycontinent.cpp \
                 src/airvpnservergroup.cpp \
                 src/cipherdatabase.cpp \
                 src/airvpnserverprovider.cpp \
                 src/semaphore.cpp \
                 src/execproc.c \
                 src/loadmod.c
                "

        case $OS_ARCH_NAME in
            i686)
                STATIC_LIB_DIR=/usr/lib/i386-linux-gnu
                SSL_LIB_DIR=/usr/local/lib
                LOCAL_LIB_DIR=./lib-i686
                break
                ;;

            x86_64)
                STATIC_LIB_DIR=/usr/lib/x86_64-linux-gnu
                SSL_LIB_DIR=/usr/local/lib
                LOCAL_LIB_DIR=./lib-x86_64
                break
                ;;

            armv7l)
                STATIC_LIB_DIR=/usr/lib/arm-linux-gnueabihf
                SSL_LIB_DIR=/usr/local/lib
                LOCAL_LIB_DIR=./lib-armv7l
                break
                ;;

            aarch64)
                STATIC_LIB_DIR=/usr/lib/aarch64-linux-gnu
                SSL_LIB_DIR=/usr/local/lib
                LOCAL_LIB_DIR=./lib-aarch64
                break
                ;;
        esac

        case $SSL_LIB_TYPE in
            OPENSSL)
                SSL_DEF=-DUSE_OPENSSL
                SSL_LIB_LINK=""
                break
                ;;

            MBEDTLS)
                SSL_DEF=-DUSE_MBEDTLS
                SSL_LIB_LINK="${SSL_LIB_DIR}/libmbedtls.a ${SSL_LIB_DIR}/libmbedx509.a ${SSL_LIB_DIR}/libmbedcrypto.a"
                break
                ;;

            *)
                echo "Unsupported SSL Library type ${SSL_LIB_TYPE}"
                exit 1
                ;;
        esac

        COMPILE="g++ -fwhole-program -Ofast -Wall -Wno-sign-compare -Wno-unused-parameter -std=c++14 -flto=4 -Wl,--no-as-needed -Wunused-local-typedefs -Wunused-variable -Wno-shift-count-overflow -pthread ${SSL_DEF} -DUSE_ASIO -DASIO_STANDALONE -DASIO_NO_DEPRECATED -I${INC_DIR} -I${ASIO}/asio/include -DHAVE_LZ4 -I${OPENVPN3} -I/usr/include/libxml2 ${DBUS_INCLUDE} ${SOURCES} -lxml2 ${DBUS_LIBS} ${LOCAL_LIB_DIR}/libcryptopp.a ${LOCAL_LIB_DIR}/libcurl.a ${SSL_LIB_LINK} -lssl -lcrypto ${STATIC_LIB_DIR}/liblz4.a ${STATIC_LIB_DIR}/libz.a ${STATIC_LIB_DIR}/liblzma.a -ldl -o ${BIN_FILE}"

        break
	    ;;

    Darwin)
        echo "Bluetit for macOS is not currently available."
        
        exit 0

        BIN_FILE=${BASE_NAME}-macos-${OS_ARCH_NAME}
        SHA_CMD="shasum -a 512"
        LIB_DIR=/usr/local/lib

        echo "Building static ${BASE_NAME} ${VERSION} for macOS ${OS_ARCH_NAME}"

        SOURCES="src/bluetit.cpp \
                 src/localnetwork.cpp \
                 src/netfilter.cpp \
                 src/execproc.c
                "

        COMPILE="clang++ -Ofast -Wall -Wno-tautological-compare -Wno-unused-private-field -Wno-c++1y-extensions -mmacosx-version-min=${MIN_MACOS_VERSION} -framework CoreFoundation -framework SystemConfiguration -framework IOKit -framework ApplicationServices -Wno-sign-compare -Wno-unused-parameter -std=c++14 -Wunused-local-typedefs -Wunused-variable -Wno-shift-count-overflow -pthread -DBOOST_ASIO_DISABLE_KQUEUE -DUSE_MBEDTLS -DUSE_ASIO -DASIO_STANDALONE -DASIO_NO_DEPRECATED -I${ASIO}/asio/include -DHAVE_LZ4 -I${OPENVPN3} $SOURCES -lz -lresolv ${LIB_DIR}/libmbedtls.a ${LIB_DIR}/libmbedx509.a ${LIB_DIR}/libmbedcrypto.a ${LIB_DIR}/liblz4.a -o $BIN_FILE"

	    break
	    ;;

    *)
	    echo "Unsupported system"
        exit 1
	    ;;
esac

OUT_DIR_NAME=${BIN_FILE}-${VERSION}
TAR_FILE_NAME=${OUT_DIR_NAME}.tar.gz
TAR_CHK_FILE_NAME=${TAR_FILE_NAME}.sha512

echo "SSL Library is ${SSL_LIB_TYPE}"

echo

echo "Compiling sources"

$COMPILE

if [ -d $OUT_DIR_NAME ]
then
    rm -r ${OUT_DIR_NAME}
fi

mkdir ${OUT_DIR_NAME}

strip ${BIN_FILE}

echo

echo "Done compiling ${BIN_FILE}"

echo

echo "Create tar file for distribution"

echo

cp ${BIN_FILE} ${OUT_DIR_NAME}/${BASE_NAME}
cp README.md ${OUT_DIR_NAME}
cp LICENSE.md ${OUT_DIR_NAME}
cp Changelog-Bluetit.txt ${OUT_DIR_NAME}/Changelog.txt

cd ${OUT_DIR_NAME}
$SHA_CMD ${BASE_NAME} > ${BASE_NAME}.sha512
cd ..

if [ -f ${TAR_FILE_NAME} ]
then
    rm ${TAR_FILE_NAME}
fi

tar czf ${TAR_FILE_NAME} ${OUT_DIR_NAME}

if [ -f ${TAR_CHK_FILE_NAME} ]
then
    rm ${TAR_CHK_FILE_NAME}
fi

$SHA_CMD ${TAR_FILE_NAME} > ${TAR_CHK_FILE_NAME}

rm -r ${OUT_DIR_NAME}

echo "tar file: ${TAR_FILE_NAME}"
echo "tar checksum file: ${TAR_CHK_FILE_NAME}"
echo
echo "Done."
